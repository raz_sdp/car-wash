<?php

namespace App\Models\rc;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Rccar extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
	protected $fillable=[
	'cbarcode',
	'rccompany_id'
	];

	public function rccompany(){
		return $this->belongsTo('App\Models\rc\Rccompany');
	}
	public function rcprocesses(){
		return $this->hasMany('App\Models\rc\Rcprocess');
	}
	public function rcsubprocess(){
		return $this->hasMany('App\Models\rc\Rcsubprocess');
	}
}
