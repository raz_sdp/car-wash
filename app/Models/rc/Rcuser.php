<?php

namespace App\Models\rc;

//use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Rcuser extends Authenticatable
{
    protected $fillable=[
		'role',
		'password',
		'acstatus',
		'email',
		'ubarcode',
		'assignedby',
		'rcemployee_id',
	];

	public function rcemployee(){
		return $this->belongsTo('App\Models\rc\Rcemployee');
	}
	public function rcattendances(){
		return $this->hasMany('App\Models\rc\Rcattendance');
	}
	public function rcprocesses(){
		return $this->hasMany('App\Models\rc\Rcprocesses');
	}
	public function rcdepartment(){
		return $this->belongsTo('App\Models\rc\Rcdepartment');
	}
	public function rcdailytasks(){
		return $this->hasMany('App\Models\rc\Rcdailytask');
	}
	public function rcshifts(){
		return $this->hasMany('App\Models\rc\Rcshift');
	}
	public function rcdutys(){
		return $this->hasMany('App\Models\rc\Rcduty');
	}
}
