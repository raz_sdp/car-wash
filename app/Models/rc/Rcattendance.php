<?php

namespace App\Models\rc;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Rcattendance extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
	protected $fillable=[
	'rcemployees_id',
	'status',
	'assignedby'
	];

	public function rcuser(){
		return $this->belongsTo('App\Models\rc\Rcuser','assignedby', 'id');
	}
	public function rcemployee(){
		return $this->belongsTo('App\Models\rc\Rcemployees');
	}
}
