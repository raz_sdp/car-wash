<?php

namespace App\Models\rc;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Rcdepartment extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
	protected $fillable=[
	 'name'
	];

	public function rcusers(){
		return $this->hasMany('App\Models\rc\Rcuser');
	}
}
