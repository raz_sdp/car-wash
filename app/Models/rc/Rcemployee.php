<?php

namespace App\Models\rc;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Rcemployee extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
	protected $fillable=[
		'ubarcode',
		'fname',
		'lname',
		'address',
		'country',
		'dob',
		'phonenumber',
		'gender',
		'email',
		'rcdepartment_id',
		'joindate',
		'avatar',
        'about'
	];


    public function rcuser(){
    	return $this->hasOne('App\Models\rc\Rcuser');
    }
	public function rcattendances(){
	return $this->hasMany('App\Models\rc\Rcattendanc');
	}
	public function rcdailytasks(){
		return $this->hasMany('App\Models\rc\Rcdailytask');
	}

	public function rcdepartment(){
		return $this->belongsTo('App\Models\rc\Rcdepartment');
	}
}
