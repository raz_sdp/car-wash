<?php

namespace App\Models\rc;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Rcshift extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
	protected $fillable=[
	 'name', 
	 'assignedby',
	 'start',
	 'end'
	];

	public function rcuser(){
		return $this->belongsTo('App\Models\rc\Rcuser','assignedby', 'id');
	}
	public function rcdailytasks(){
		return $this->hasMany('App\Models\rc\Rcdailytask');
	}
}
