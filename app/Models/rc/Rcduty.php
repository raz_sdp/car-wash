<?php

namespace App\Models\rc;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Rcduty extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
	protected $fillable=[
	 'workarea', 
	 'assignedby'
	];
	public function rcuser(){
		return $this->belongsTo('App\Models\rc\Rcuser');
	}
	public function rcshift(){
		return $this->hasMany('App\Models\rc\Rcshift');
	}
	public function assignedby(){
		return $this->belongsTo('App\Models\rc\Rcuser','assignedby', 'id');
	}
}
