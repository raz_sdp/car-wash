<?php

namespace App\Models\rc;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Rcprocesstype extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
	protected $fillable=[
	 'name'
	];

	public function rcprocesses(){
		return $this->hasOne('App\Models\rc\Rccprosess');
	}
	public function rcsubprocess(){
		return $this->hasOne('App\Models\rc\Rcsubprocess');
	}
}
