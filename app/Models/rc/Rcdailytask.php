<?php

namespace App\Models\rc;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Rcdailytask extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
	protected $fillable=[
		'rcduty_id',
		'rcshif_id',
		'assignedby',
		'rcemployee_id',
		'ubarcode'
	];

	public function rcemploye(){
		return $this->belongsTo('App\Models\rc\Rcemploye');
	}
	public function rcshift(){
		return $this->belongsTo('App\Models\rc\Rcshift');
	}
	public function rcduty(){
		return $this->belongsTo('App\Models\rc\Rcduty');
	}
	public function rcuser(){
		return $this->belongsTo('App\Models\rc\Rcuser','assignedby', 'id');
	}
}
