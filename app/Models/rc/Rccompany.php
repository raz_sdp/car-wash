<?php

namespace App\Models\rc;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Rccompany extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
	protected $fillable=[
		'name', 
		'country',
		'state',
		'email',
		'streetone',
		'streettwo',
		'postcode',
		'website',
		'phonenumber',
		'description'
	];

	public function rccars(){
		return $this->hasMany('App\Models\rc\Rccar');
	}
}
