<?php

namespace App\Models\rc;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Rcprocess extends Model
{
    use SoftDeletes;
    //protected $dates = ['deleted_at'];
	protected $fillable=[
		'rccar_id',
		'rcprocesstype_id',
		'assignedby',
		'in',
		'out',
		'status',
		'rcuser_id',
		'ubarcode',
		'currentprocess'
	];

    public function rccar(){
        return $this->belongsTo('App\Models\rc\Rccar');
    }
	public function rcuser(){
		return $this->belongsTo('App\Models\rc\Rcuser');
	}
	public function rcprocesstype(){
		return $this->belongsTo('App\Models\rc\Rcprocesstype');
	}
	public function assignedby(){
		return $this->belongsTo('App\Models\rc\Rcuser','assignedby', 'id');
	}
}
