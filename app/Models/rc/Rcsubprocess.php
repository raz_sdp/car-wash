<?php

namespace App\Models\rc;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Rcsubprocess extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
	protected $fillable=[
		'rcprocesstype_id',
		'name'
	];

	public function rcprocesstype(){
		return $this->belongTo('App\Models\rc\Rcprocesstype');
	}
}
