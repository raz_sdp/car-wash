<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Operator
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
   public function handle($request, Closure $next, $lang=null)
    {
         //langset($lang);
        langset('en');
        if(Auth::user() && Auth::user()->role == 3 ){
            return $next($request);
        }
        
        return redirect()->route('dashboard', ['lang'=>$lang]);
    }
}
