<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Team
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
     public function handle($request, Closure $next, $lang=null)
    {
        //langset($lang);
        langset('en');
        if(Auth::user()){
            return $next($request);
        }
        return redirect()->route('login', ['lang'=>$lang]);
    }
}
