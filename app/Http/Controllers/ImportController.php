<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Excel;
use Input;
use App\Models\rc\Rccar;
use App\Models\rc\Rcemployee;
use App\Models\rc\Rccompany;
use Carbon\Carbon;
use Exception;
class ImportController extends Controller
{
   public function import($lang=null)
    {
      //$default = $data['adm_setting'] = allsetting();
      //langset($lang, $default['lang']);
      langset($lang, 'en');
        $data['lang'] = $lang;

      return view('import',$data);
    }
    public function importprocess( $model, Request $request, $lang=null ){
      langset($lang, 'en');
        $data['lang'] = $lang;
        // dd($request->files);
        $file = Input::file('files');
        $file_name = $file->getClientOriginalName();
        $file->move('file',$file_name);
        Excel::load('file/'.$file_name,function ($reader) use($request, $model){
          $results = $reader->toArray();
          $a=0;
          foreach ($results[0] as $result)
          {
            $result['created_at']=Carbon::now();
            $result['updated_at']=Carbon::now();
            $mydata[] = $result;
            $a++;
          }
         // dd($results);
        // dd($mydata);
          try {
            if($model =='user'){
               $employee=Rcemployee::insert($mydata);
              if($employee){ 
                return redirect()->back()->with(['success'=> 'Successfully add employee.'])->send(); 
              }
              else{
                return redirect()->back()->with(['dismiss'=> 'Fail to register employee.'])->send();
              }  
            } 
            else if($model =='company'){
               $employee=Rccompany::insert($mydata);
              if($employee){ 
                return redirect()->back()->with(['success'=> 'Successfully add employee.'])->send(); 
              }
              else{
                return redirect()->back()->with(['dismiss'=> 'Fail to register employee.'])->send();
              }  
            } 
              else{
                echo 2;
            }
          } 
          catch (Exception $e) {
            return redirect()->back()->with(['dismiss'=> 'problem in uploading'])->send(); 
          }
        })->get();
        

    // return back();
    }

}
