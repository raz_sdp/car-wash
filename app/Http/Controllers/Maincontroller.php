<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\rc\Rccar;

class Maincontroller extends Controller
{
     function home($lang=null)
    {
        $default = $data['adm_setting'] = allsetting();
        langset($lang, $default['lang']);
        $data['lang'] = $lang;
        
        return view('common.home', $data);
    }

    function getcardetails(Request $request){
        $cbarcode = $request->cbarcode;
        $car=Rccar::where(['cbarcode'=>$cbarcode])->first();
        $cardetails=$car->rccompany;
        return response()->json(['cardetails'=>$cardetails]);
    }

}
