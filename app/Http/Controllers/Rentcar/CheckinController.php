<?php

namespace App\Http\Controllers\Rentcar;

use App\Models\rc\Rccar;
use App\Models\rc\Rcprocess;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CheckinController extends Controller
{
    function checkin($lang=null)
    {
        $default = $data['adm_setting'] = allsetting();
        langset($lang, $default['lang']);
        $data['lang'] = $lang;
        
        return view('rentcar.checkin', $data);
    }

    function checkincarlist($lang=null)
    {
        $default = $data['adm_setting'] = allsetting();
        langset($lang, $default['lang']);
        $data['lang'] = $lang;

        $data['car_list'] = Rcprocess::where('status','!=','5')->groupBy('rccar_id')->get();
        
        return view('rentcar.checkincarlist', $data);
    }

    function singlecheckin($car_id = null, $lang = null)
    {
        $default = $data['adm_setting'] = allsetting();
        langset($lang, $default['lang']);
        $data['lang'] = $lang;
        $data['car_data'] = Rccar::where('id',$car_id)->first();
        #dd($data['car_data']->rccompany);
        $data['process_types'] = Rcprocess::where('rccar_id',$car_id)->select('rcprocesstype_id')->get();
        return view('rentcar.singlecheckin', $data);
    }
}
