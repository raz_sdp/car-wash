<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Models\rc\Rccompany;
use App\Models\rc\Rccar;

class RccarController extends Controller
{
    public function regcar($lang=null){
    	langset($lang, 'en');
    	$data['lang'] = $lang;

    	$data['companies']= Rccompany::get();
    	return view('administrator.regcar',$data);
    }

    public function regcarprocess(Request $request,$lang=null){
    	langset($lang, 'en');
    	$data['lang'] = $lang;
    	
		$rules = [ 
	      'cbarcode'=>'required|max:255',
	      'company' => 'required|integer',
	    ];

	    $messages = [ 
	      'cbarcode.required' => 'Barcode can not be empty',
	      'cbarcode.max' => 'Barcode must not exit 255',
	      'company.required' => 'Company can not be empty',
	      'company.integer' => 'Enter a valid Company',
	    ];

		$validator = Validator::make($request->all(), $rules, $messages);
		if ($validator->fails()) {
	      return redirect()->back()->withErrors($validator)->withInput();
	    }
		    else {
		       $carinput['cbarcode']=$request->cbarcode;
		       $carinput['rccompany_id']=$request->company;
		     
		       $inserted=Rccar::create($carinput)->id; 

		    if($inserted){
		      return redirect()->back()->with(['success'=> 'Successfully add a new car.']);
	    	}
		    else{
		      return redirect()->back()->with(['dismiss'=> 'Fail to register car.']);
		    }  
		  }
    }

    public function viewallcar($lang=null){
      
      $allsetting = $data['allsetting'] = allsetting();
      langset($lang, $allsetting['lang']);
      $data['lang'] = $lang;
      
        $order  = \Request::get('ord');
        $col = \Request::get('sort');
        $search = \Request::get('srch');
        $frm = \Request::get('frm');
        $to = \Request::get('to');
        if($order== 'a'){$order = 'asc';}else{$order=='desc';}

        $allcol = ['i'=>'id','com'=>'rccompany_id'];
        $data['getelements'] = ['srch' => $search,'ord' => $order,'sort' => $col,'frm' => $frm,'to' => $to];
        
        $column = colquery($allcol);
        $srcharr = explode(' ', $search);
        

        foreach($data['getelements'] as $key => $val){
            if ($val == ''){
                unset($data['getelements'][$key]);
            }
             elseif($key=='frm'|| $key=='to'){
                 if(chdate($val)==false){
                     unset($data['getelements'][$key]);
                 }
             }
        }

        $a = new Rccar();
        if(isset($data['getelements']['frm'])){
            $a = $a->where('created_at','>=',carbondate($frm));
        }
        if(isset($data['getelements']['to'])){
            $a = $a->where('created_at','<',carbonext($to));
        }
        else{
            $a = $a->where('created_at','<',carbonext(date('Y-m-d')));
        }
        if(!empty($search)){
            $curr = 1;
            $a = $a->where(function($query) use($srcharr,$curr){
                    foreach ($srcharr as $wh){
                        if($curr==1){
                            $query->where('id','like','%'.$wh.'%');
                        }else {
                            $query->orWhere('id','like','%'.$wh.'%');
                        }
                        $query->orWhere('rccompany_id','like','%'.$wh.'%');
                        $curr =0;
                    }
                });
        }
        $data['cars'] = $a->orderBy($column, $order)->paginate($allsetting['item_perpage'], ['*'], 'p')->appends($data['getelements']);
        return view('administrator.viewallcar',$data);
    }

}
