<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Models\rc\Rccompany;
use App\Models\rc\Rccar;

class RccompanyController extends Controller
{
    public function regcompany($lang=null){
        langset($lang, 'en');
        $data['lang'] = $lang;

        return view('administrator.regcompany',$data);
    }
    public function regcompanyprocess(Request $request,$lang=null){
        langset($lang, 'en');
        $data['lang'] = $lang;
        
        $rules = [ 
          'name'=>'required|max:255',
          'website'=>'max:255',
          'email'=>'required|email|unique:rccompanies',
          'phonenumber'=>'required|unique:rccompanies',
          'country'=>'required|in:'.arrKeyOnly(country()),
          'postcode'=>'required|max:255',
          'state'=>'max:255'
        ];

        $messages = [ 
          'name.required' => 'First Name field can not be empty',
          'website.max' => 'Website must not exit 255',
          'email.required' => 'Email field can not be empty',
          'email.email' => 'Enter a valid email',
          'email.unique' => 'Email Address already exists',
          'phonenumber.required' => 'Phone field can not be empty',
          'phonenumber.unique' => 'Phone already exists',
          'country.required' => 'Country field can not be empty',
          'country.in' => 'Invalid Country',
          'state.required' => 'state field can not be empty'
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
          return redirect()->back()->withErrors($validator)->withInput();
        }
            else {
               $companyinput['name']=$request->name;
               $companyinput['website']=$request->website;
               $companyinput['email']=$request->email;
               $companyinput['phonenumber']=$request->phonenumber;
               $companyinput['country']=$request->country;
               $companyinput['postcode']=$request->postcode;  
               $companyinput['state']=$request->state;  
               $companyinput['streetone']=$request->streetone;  
               $companyinput['streettwo']=$request->streettwo;
               $inserted=Rccompany::create($companyinput)->id; 

            if($inserted){
              return redirect()->back()->with(['success'=> 'Successfully add company.']);
            }
            else{
              return redirect()->back()->with(['dismiss'=> 'Fail to register company.']);
            }  
          }
    }

    public function viewallcompany($lang=null){
      
      $allsetting = $data['allsetting'] = allsetting();
      langset($lang, $allsetting['lang']);
      $data['lang'] = $lang;

      
        $order  = \Request::get('ord');
        $col = \Request::get('sort');
        $search = \Request::get('srch');
        $frm = \Request::get('frm');
        $to = \Request::get('to');
        if($order== 'a'){$order = 'asc';}else{$order=='desc';}

        $allcol = ['i'=>'id','n'=>'name','c'=>'country','em'=>'email','mb'=>'phonenumber','so'=>'streetone','st'=>'streettwo','p'=>'postcode','w'=>'website','s'=>'state'];
        $data['getelements'] = ['srch' => $search,'ord' => $order,'sort' => $col,'frm' => $frm,'to' => $to];
        
        $column = colquery($allcol);
        $srcharr = explode(' ', $search);
        

        foreach($data['getelements'] as $key => $val){
            if ($val == ''){
                unset($data['getelements'][$key]);
            }
             elseif($key=='frm'|| $key=='to'){
                 if(chdate($val)==false){
                     unset($data['getelements'][$key]);
                 }
             }
        }

        $a = new Rccompany();
        if(isset($data['getelements']['frm'])){
            $a = $a->where('created_at','>=',carbondate($frm));
        }
        if(isset($data['getelements']['to'])){
            $a = $a->where('created_at','<',carbonext($to));
        }
        else{
            $a = $a->where('created_at','<',carbonext(date('Y-m-d')));
        }
        if(!empty($search)){
            $curr = 1;
            $a = $a->where(function($query) use($srcharr,$curr){
                    foreach ($srcharr as $wh){
                        if($curr==1){
                            $query->where('id','like','%'.$wh.'%');
                        }else {
                            $query->orWhere('id','like','%'.$wh.'%');
                        }
                        $query->orWhere('name','like','%'.$wh.'%');
                        $query->orWhere('country','like','%'.$wh.'%');
                        $query->orWhere('email','like','%'.$wh.'%');
                        $query->orWhere('phonenumber','like','%'.$wh.'%');
                        $query->orWhere('streetone','like','%'.$wh.'%');
                        $query->orWhere('streettwo','like','%'.$wh.'%');
                        $query->orWhere('postcode','like','%'.$wh.'%');
                        $query->orWhere('website','like','%'.$wh.'%');
                        $query->orWhere('state','like','%'.$wh.'%');
                        $curr =0;
                    }
                });
        }
        $data['companies'] = $a->orderBy($column, $order)->paginate($allsetting['item_perpage'], ['*'], 'p')->appends($data['getelements']);
        return view('administrator.viewallcompany',$data);
    }

    public function company($id,$lang=null){
        langset($lang, 'en');
        $data['lang'] = $lang;
        $data['id'] = $id;
        $data['company'] = Rccompany::where(['id'=>$id])->first();
        $data['cars'] = Rccar::where(['rccompany_id'=>$id])->get();
        $data['totalcars'] = Rccar::where(['rccompany_id'=>$id])->count();
        return view('administrator.company',$data);
    }



    //update comapny information
    public function companyUpdateProcess(Request $request,$id,$lang=null){
        langset($lang, 'en');
        $data['lang'] = $lang;

        $rules = [ 
          'name'=>'required|max:255',
          'website'=>'max:255',
          'email'=>'required|email',
          'phonenumber'=>'required',
          'country'=>'required|in:'.arrKeyOnly(country()),
          'postcode'=>'required|max:255',
          'state'=>'max:255'
        ];

        $messages = [ 
          'name.required' => 'First Name field can not be empty',
          'website.max' => 'Website must not exit 255',
          'email.required' => 'Email field can not be empty',
          'email.email' => 'Enter a valid email',
          'phonenumber.required' => 'Phone field can not be empty',
          'country.required' => 'Country field can not be empty',
          'country.in' => 'Invalid Country',
          'state.required' => 'state field can not be empty'
        ];

        $company = Rccompany::where(['id'=>$id])->first();
        if($request->email != $company->email){
            $rules['email'] = 'required|email|unique:rccompanies';
            $messages['email.unique'] = 'Email Address already exists';
        }
        if($request->phonenumber != $company->phonenumber){
            $rules['phonenumber'] = 'required|unique:rccompanies';
            $messages['phonenumber.unique'] = 'Phone already exists';
        }

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
          return redirect()->back()->withErrors($validator)->withInput();
        }
        else {
               $companyinput['name']=$request->name;
               $companyinput['website']=$request->website;
               $companyinput['email']=$request->email;
               $companyinput['phonenumber']=$request->phonenumber;
               $companyinput['country']=$request->country;
               $companyinput['postcode']=$request->postcode;  
               $companyinput['state']=$request->state;  
               $companyinput['streetone']=$request->streetone;  
               $companyinput['streettwo']=$request->streettwo;
               $update=Rccompany::where(['id'=>$id])->update($companyinput); 

            if($update){
              return redirect()->back()->with(['success'=> 'Successfully update.']);
            }
            else{
              return redirect()->back()->with(['dismiss'=> 'Fail to update.']);
            }  
      }

    }

}