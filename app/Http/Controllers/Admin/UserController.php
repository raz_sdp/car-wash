<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\rc\Rcdepartment;
use App\Models\rc\Rcemployee;
use App\Models\rc\Rcuser;
use Validator;
Use Hash;


class UserController extends Controller
{
  public function addemployee($lang=null)
    {
	    $allsetting = $data['allsetting'] = allsetting();
      langset($lang, $allsetting['lang']);
      $data['lang'] = $lang;

	    $data['departments']= Rcdepartment::get();
	    return view('administrator.addemployee',$data);
    }

  public function addemployeeprocess(Request $request, $lang=null)
  {
      $allsetting = $data['allsetting'] = allsetting();
      langset($lang, $allsetting['lang']);
      $data['lang'] = $lang;
    
    $rules = [ 
      'fname'=>'required|max:255',
      'lname'=>'max:255',
      'email'=>'required|email|unique:rcemployees',
      'phonenumber'=>'required|unique:rcemployees',
      'country'=>'required|in:'.arrKeyOnly(country()),
      'address'=>'required|max:255',
      'dob'=>'date',
      'gender' => 'required|integer|min:1|max:'.count(gender()),
      'jdate'=>'date'
    ];
    $messages = [ 
      'fname.required' => 'First Name field can not be empty',
      'fname.max' => 'First Name must not exit 255',
      'lname.max' => 'Last Name must not exit 255',
      'email.required' => 'Email field can not be empty',
      'email.email' => 'Enter a valid email',
      'email.unique' => 'Email Address already exists',
      'phonenumber.required' => 'Phone field can not be empty',
      'phonenumber.unique' => 'Phone already exists',
      'country.required' => 'Country field can not be empty',
      'country.in' => 'Invalid Country',
      'address.required' => 'Address field can not be empty',
      'address.max' => 'Address must not exit 255',
      'dob.date'=>'Invalid date of birth',
      'gender.required' => 'Gender field can not be empty',
      'gender.integer' => 'Invalid gender',
      'gender.min' => 'Invalid gender',
      'gender.max' => 'Invalid gender',
      'dob.date'=>'Invalid joint date'
    ];
    $validator = Validator::make($request->all(), $rules, $messages);

    if ($validator->fails()) {
      return redirect()->back()->withErrors($validator)->withInput();
    }
    else {
       $userinput['fname']=$request->fname;
       $userinput['lname']=$request->lname;
       $userinput['email']=$request->email;
       $userinput['phonenumber']=$request->phonenumber;
       $userinput['country']=$request->country;
       $userinput['address']=$request->address;  
       $userinput['dob']=$request->dob;  
       $userinput['gender']=$request->gender;  
       $userinput['rcdepartment_id']=$request->department;
       $userinput['joindate']=$request->jdate;
       $userinput['ubarcode'] = md5($request->phonenumber.uniqid().randomString(2)); 
      // dd($userinput);
       $inserted=Rcemployee::create($userinput)->id; 

       if($inserted)
        {
           if($request->user == 1){
              
              $input['role']=$request->role;
              $input['password']= Hash::make($request->password);
              $input['email']=$request->email; 
              $input['assignedby']=Auth::user()->id;
              $input['rcemployee_id']=$inserted; 
              $input['ubarcode'] = md5($request->phonenumber.uniqid().randomString(2)); 

            $insertuser= Rcuser::create($input); 
            if($insertuser)
            {
              return redirect()->back()->with(['success'=> 'Successfully add user.']); 
            } 
           else{
              return redirect()->back()->with(['dismiss'=> 'Fail to create user.']);
           }   
        } 
       return redirect()->back()->with(['success'=> 'Successfully add employee.']); 
      }
      else{
       return redirect()->back()->with(['dismiss'=> 'Fail to register employee.']);
      }  
    }
  }

  public function editemployeeprocess(Request $request, $lang=null)
  {
      $allsetting = $data['allsetting'] = allsetting();
      langset($lang, $allsetting['lang']);
      $data['lang'] = $lang;

      $user = Rcemployee::where(['id'=>$request->id])->first();
      if($user){

          $rules = [ 
            'fname'=>'required|max:255',
            'lname'=>'max:255',
            'country'=>'required|in:'.arrKeyOnly(country()),
            'address'=>'required|max:255',
            'dob'=>'date',
            'gender' => 'required|integer|min:1|max:'.count(gender()),
            'jdate'=>'date'
          ];
          $messages = [ 
            'fname.required' => 'First Name field can not be empty',
            'fname.max' => 'First Name must not exit 255',
            'lname.max' => 'Last Name must not exit 255',
            'country.required' => 'Country field can not be empty',
            'country.in' => 'Invalid Country',
            'address.required' => 'Address field can not be empty',
            'address.max' => 'Address must not exit 255',
            'dob.date'=>'Invalid date of birth',
            'gender.required' => 'Gender field can not be empty',
            'gender.integer' => 'Invalid gender',
            'gender.min' => 'Invalid gender',
            'gender.max' => 'Invalid gender',
            'dob.date'=>'Invalid joint date'
          ];

          if($request->email != $user->email){
            $rules['email'] = 'required|email|unique:rccompanies';
            $messages['email.unique'] = 'Email Address already exists';
          }
          if($request->phonenumber != $user->phonenumber){
            $rules['phonenumber'] = 'required|unique:rcemployees';
            $messages['phonenumber.unique'] = 'Email Address already exists';
          }
          $validator = Validator::make($request->all(), $rules, $messages);

          if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
          }
          else {
             $userinput['fname']=$request->fname;
             $userinput['lname']=$request->lname;
             $userinput['email']=$request->email;
             $userinput['phonenumber']=$request->phonenumber;
             $userinput['country']=$request->country;
             $userinput['address']=$request->address;  
             $userinput['dob']=$request->dob;  
             $userinput['gender']=$request->gender;  
             $userinput['joindate']=$request->jdate;
             $userinput['about']=$request->about;
             
             $update = Rcemployee::where(['id'=>$request->id])->update($userinput);

             if($update)
              {
                 if(!isset($user->rcuser) && (count($user->rcuser) <= 0)){
                    
                    $input['email']=$request->email; 
                    Rcuser::where(['rcemployee_id'=>$request->id])->update($input); 
                    
              } 
             return redirect()->back()->with(['success'=> 'Successfully update employee information.']); 
            }
            else{
             return redirect()->back()->with(['dismiss'=> 'Fail to update employee information.']);
            }  
          }
      }
      else{
         return redirect()->back()->with(['dismiss'=> 'Not an employee.']);
      }    
  }
  public function viewallemployee($lang=null)
    {
      $allsetting = $data['allsetting'] = allsetting();
      langset($lang, $allsetting['lang']);
      $data['lang'] = $lang;

      // $bc = [
      //       ['dashboard','Dashboard', 'home']
      //   ];
        // $data['breadcrumb'] = breadcrumb($bc, $allsetting['breadcrumb_seperator']);

        $order  = \Request::get('ord');
        $col = \Request::get('sort');
        $search = \Request::get('srch');
        $frm = \Request::get('frm');
        $to = \Request::get('to');
        if($order== 'a'){$order = 'asc';}else{$order=='desc';}

        $allcol = ['i'=>'id','fn'=>'fname','ln'=>'lname','em'=>'email','mb'=>'phonenumber','j'=>'joindate'];
        $data['getelements'] = ['srch' => $search,'ord' => $order,'sort' => $col,'frm' => $frm,'to' => $to];
        
        $column = colquery($allcol);
        $srcharr = explode(' ', $search);
        

        foreach($data['getelements'] as $key => $val){
            if ($val == ''){
                unset($data['getelements'][$key]);
            }
             elseif($key=='frm'|| $key=='to'){
                 if(chdate($val)==false){
                     unset($data['getelements'][$key]);
                 }
             }
        }

        $a = new Rcemployee();
        if(isset($data['getelements']['frm'])){
            $a = $a->where('created_at','>=',carbondate($frm));
        }
        if(isset($data['getelements']['to'])){
            $a = $a->where('created_at','<',carbonext($to));
        }
        else{
            $a = $a->where('created_at','<',carbonext(date('Y-m-d')));
        }
        if(!empty($search)){
            $curr = 1;
            $a = $a->where(function($query) use($srcharr,$curr){
                    foreach ($srcharr as $wh){
                        if($curr==1){
                            $query->where('id','like','%'.$wh.'%');
                        }else {
                            $query->orWhere('id','like','%'.$wh.'%');
                        }
                        $query->orWhere('fname','like','%'.$wh.'%');
                        $query->orWhere('lname','like','%'.$wh.'%');
                        $query->orWhere('email','like','%'.$wh.'%');
                        $query->orWhere('phonenumber','like','%'.$wh.'%');
                        $query->orWhere('joindate','like','%'.$wh.'%');
                        $curr =0;
                    }
                });
        }
        $data['employees'] = $a->orderBy($column, $order)->paginate($allsetting['item_perpage'], ['*'], 'p')->appends($data['getelements']);
      return view('administrator.viewallemployee',$data);
    }

  //  public function editemployee($id, $lang=null)
  // {
  //   //$default = $data['adm_setting'] = allsetting();
  //   //langset($lang, $default['lang']);
  //   langset($lang, 'en');
  //   $data['lang'] = $lang;

  //   $data['user']= Rcemployee::where(['id'=>$id])->first();
  //   return view('admin.editemployeeinfo',$data);
  // }
     
      public function makeuser($id, $lang=null)
  {
      $allsetting = $data['allsetting'] = allsetting();
      langset($lang, $allsetting['lang']);
      $data['lang'] = $lang;

    $data['id']= $id;
    return view('administrator.makeuser',$data);
  }

  public function viewalluser($lang=null)
    {
      $allsetting = $data['allsetting'] = allsetting();
      langset($lang, $allsetting['lang']);
      $data['lang'] = $lang;

      // $bc = [
      //       ['dashboard','Dashboard', 'home']
      //   ];
        // $data['breadcrumb'] = breadcrumb($bc, $allsetting['breadcrumb_seperator']);

        $order  = \Request::get('ord');
        $col = \Request::get('sort');
        $search = \Request::get('srch');
        $frm = \Request::get('frm');
        $to = \Request::get('to');
        if($order== 'a'){$order = 'asc';}else{$order=='desc';}

        $allcol = ['id'=>'id', 'r'=>'Role',];
        $data['getelements'] = ['srch' => $search,'ord' => $order,'sort' => $col,'frm' => $frm,'to' => $to];
        
        $column = colquery($allcol);
        $srcharr = explode(' ', $search);
        

        foreach($data['getelements'] as $key => $val){
            if ($val == ''){
                unset($data['getelements'][$key]);
            }
             elseif($key=='frm'|| $key=='to'){
                 if(chdate($val)==false){
                     unset($data['getelements'][$key]);
                 }
             }
        }

        $a = new Rcuser();
        if(isset($data['getelements']['frm'])){
            $a = $a->where('created_at','>=',carbondate($frm));
        }
        if(isset($data['getelements']['to'])){
            $a = $a->where('created_at','<',carbonext($to));
        }
        else{
            $a = $a->where('created_at','<',carbonext(date('Y-m-d')));
        }
        if(!empty($search)){
            $curr = 1;
            $a = $a->where(function($query) use($srcharr,$curr){
                    foreach ($srcharr as $wh){
                        if($curr==1){
                            $query->where('id','like','%'.$wh.'%');
                        }else {
                            $query->orWhere('id','like','%'.$wh.'%');
                        }
                        $query->orWhere('role','like','%'.$wh.'%');
                        $query->orWhere('email','like','%'.$wh.'%');
                        $curr =0;
                    }
                });
        }
        $data['users'] = $a->orderBy($column, $order)->paginate($allsetting['item_perpage'], ['*'], 'p')->appends($data['getelements']);

        return view('administrator.viewalluser',$data);  
    }
    

  public function makeuserprocess(Request $request, $lang=null)
  {
      $allsetting = $data['allsetting'] = allsetting();
      langset($lang, $allsetting['lang']);
      $data['lang'] = $lang;
     $employee = Rcuser::where(['rcemployee_id'=>$request->id])->count();
    if($employee>0){
       return redirect()->back()->with(['dismiss'=> 'Already a user.']);
    }
    else{
       
          $rules = [ 
            'password'=>'required|max:255',
            'email'=>'required|email|unique:rcusers',
            'role' => 'required|integer|min:1|max:'.count(role()),
         
          ];
          $messages = [ 
            'fname.required' => 'First Name field can not be empty',
            'fname.max' => 'First Name must not exit 255',
            'lname.max' => 'Last Name must not exit 255',
            'email.required' => 'Email field can not be empty',
            'email.email' => 'Enter a valid email',
            'email.unique' => 'Email Address already exists',
            'phonenumber.required' => 'Phone field can not be empty',
            'phonenumber.unique' => 'Phone already exists',
            'country.required' => 'Country field can not be empty',
            'country.in' => 'Invalid Country',
            'address.required' => 'Address field can not be empty',
            'address.max' => 'Address must not exit 255',
            'dob.date'=>'Invalid date of birth',
            'gender.required' => 'Gender field can not be empty',
            'gender.integer' => 'Invalid gender',
            'gender.min' => 'Invalid gender',
            'gender.max' => 'Invalid gender',
            'dob.date'=>'Invalid joint date'
          ];
          $validator = Validator::make($request->all(), $rules, $messages);

          if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
          }
          else {
                    
                    $input['role']=$request->role;
                    $input['password']= Hash::make($request->password);
                    $input['email']=$request->email; 
                    $input['assignedby']=Auth::user()->id;
                    $input['rcemployee_id']=$request->id; 
                    $input['ubarcode'] = md5($request->phonenumber.uniqid().randomString(2)); 

                  $insertuser= Rcuser::create($input); 
                  if($insertuser)
                  {
                    return redirect()->back()->with(['success'=> 'Successfully add user.']); 
                  } 
                 else{
                    return redirect()->back()->with(['dismiss'=> 'Fail to create user.']);
                 }   
           
          }

      }
 
  }  

   public function employee( $id,$lang=null )
    {
      $allsetting = $data['allsetting'] = allsetting();
      langset($lang, $allsetting['lang']);
      $data['lang'] = $lang;

      $data['employee']=Rcemployee::where(['id'=>$id])->first();
      return view('administrator.employee',$data);
    }

}
