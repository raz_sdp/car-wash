<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\rc\Rcemployee;
use App\Models\rc\Rcuser;
use Validator;


class Authcontroller extends Controller
{
    function login($lang=null){

      //$default = $data['adm_setting'] = allsetting();
      //langset($lang, $default['lang']);
        langset($lang, 'en');
        $data['lang'] = $lang;

      return view('common.login',$data);
    }

    function loginprocess(Request $request, $lang=null){

        //$default = $data['adm_setting'] = allsetting();
        //langset($lang, $default['lang']);
        langset($lang, 'en');
        $data['lang'] = $lang;

        $rules = [ 

            'email'=>'required',
            'password'=>'required'
        ];

        $messages = [ 

            'user.required' => 'User field can not be empty',
            'password.required' => 'password field can not be empty',
            
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) 
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        else {
          
          if(Auth::attempt(['ubarcode'=>$request->ubarcode, 'email'=>$request->email, 'password'=>$request->password], $request->has('remember'))){
            
              return redirect()->route('dashboard', ['lang'=>$lang])->with(['success'=> 'Login is successfull.']);
            }
            
          else
          {
              return redirect()->back()->with(['dismiss'=> 'Wrong Email or Password']);
          }
        }  
    }

    function logout($lang=null)
    {
        /**
         * must langset($lang, $default['lang']) function will be above all code 
         */
       // $default = $data['adm_setting'] = allsetting();
        //langset($lang, $default['lang']);
        langset($lang, 'en');
        $data['lang'] = $lang;

        if(Auth::user()){
          
            Auth::logout();
            return redirect()->route('login',['lang'=>$lang]);
        }
        else{
          return redirect()->back();
        }
    }

}
