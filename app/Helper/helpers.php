<?php
use App\Models\Adminsetting;
use Carbon\Carbon;

function allsetting($a=null){
 	$allsettings = Adminsetting::get();
 	if($allsettings){
		$output=[];
		foreach($allsettings as $setting){
			$output[$setting->slug] = $setting->value;
		}
		if($a==null){
			return $output;
		}
		else{
			return $output[$a];
		}
 	}
	return false;
}
//gender list for work
function gender($a=null){
	$sex= array(
		1 =>'Undefined',
		2 =>'Male',
		3 =>'Female'
	);
	if($a==null){
		return $sex;
	}
	else{
		return $sex[$a];
	}
}

// active level
function activelevel($active=null)
{
	$level= [
		2 =>'suspend',
		1 =>'active'
	];

	if($active==null){
		return $level;
	}
	else{
		return $level[$active];
	}
}


//Country list for work
function role($val=null){
	$myrole= array(
		1 =>'Administrator',
		2 =>'Supervisor',
		3=>'Operator'
	);
	if($val==null){
		return $myrole;
	}
	else{
		return $myrole[$val];
	}
	return $myrole;
}

function langexp(){
	$a = language();
	$output='';
	$sep='';
	foreach ($a as $value) {
		$output .= $sep.$value;
		$sep='|';
	}
	return $output;
}

//Call this in every function
function langset($lang=null, $default=null){
	// if ($lang!=null && !in_array($lang, language())){
 //        return redirect()->back()->send();
 //        // return \view::make('errors.404');
 //    }

	if($lang==null){
		// $lang = App()->getLocale();
		// dd(allsetting('lang'));
		if($default!=null){
			$lang = $default;
		}
		else{
			$lang = allsetting('lang');
		}
	}
	App()->setlocale($lang);
}



function language($a=null){
	$path = base_path('resources/lang');
	$initial = scandir($path);
	$directories = array();
	foreach($initial as $init){
		if($init !='.' && $init !='..' && $init !='default'){
			$directories[$init] =  $init;
		}
	}
	if($a==null){
		return $directories;
	}
	else{
		return $directories[$a];
	}
}

function replace_str($str,$start=0,$count=-3,$repstr="xxxxxxxx")
{
	return str_replace(substr($str,$start,$count),$repstr,$str);
}

function country($a=null){
	$output = [
		'ND' =>	trans('setting/country.ND'),
		'AF' => trans('setting/country.AF'),
		'AL' => trans('setting/country.AL'),
		'DZ' => trans('setting/country.DZ'),
		'DS' => trans('setting/country.DS'),
		'AD' => trans('setting/country.AD'),
		'AO' => trans('setting/country.AO'),
		'AI' => trans('setting/country.AI'),
		'AQ' => trans('setting/country.AQ'),
		'AG' => trans('setting/country.AG'),
		'AR' => trans('setting/country.AR'),
		'AM' => trans('setting/country.AM'),
		'AW' => trans('setting/country.AW'),
		'AU' => trans('setting/country.AU'),
		'AT' => trans('setting/country.AT'),
		'AZ' => trans('setting/country.AZ'),
		'BS' => trans('setting/country.BS'),
		'BH' => trans('setting/country.BH'),
		'BD' => trans('setting/country.BD'),
		'BB' => trans('setting/country.BB'),
		'BY' => trans('setting/country.BY'),
		'BE' => trans('setting/country.BE'),
		'BZ' => trans('setting/country.BZ'),
		'BJ' => trans('setting/country.BJ'),
		'BM' => trans('setting/country.BM'),
		'BT' => trans('setting/country.BT'),
		'BO' => trans('setting/country.BO'),
		'BA' => trans('setting/country.BA'),
		'BW' => trans('setting/country.BW'),
		'BV' => trans('setting/country.BV'),
		'BR' => trans('setting/country.BR'),
		'IO' => trans('setting/country.IO'),
		'BN' => trans('setting/country.BN'),
		'BG' => trans('setting/country.BG'),
		'BF' => trans('setting/country.BF'),
		'BI' => trans('setting/country.BI'),
		'KH' => trans('setting/country.KH'),
		'CM' => trans('setting/country.CM'),
		'CA' => trans('setting/country.CA'),
		'CV' => trans('setting/country.CV'),
		'KY' => trans('setting/country.KY'),
		'CF' => trans('setting/country.CF'),
		'TD' => trans('setting/country.TD'),
		'CL' => trans('setting/country.CL'),
		'CN' => trans('setting/country.CN'),
		'CX' => trans('setting/country.CX'),
		'CC' => trans('setting/country.CC'),
		'CO' => trans('setting/country.CO'),
		'KM' => trans('setting/country.KM'),
		'CG' => trans('setting/country.CG'),
		'CK' => trans('setting/country.CK'),
		'CR' => trans('setting/country.CR'),
		'HR' => trans('setting/country.HR'),
		'CU' => trans('setting/country.CU'),
		'CY' => trans('setting/country.CY'),
		'CZ' => trans('setting/country.CZ'),
		'DK' => trans('setting/country.DK'),
		'DJ' => trans('setting/country.DJ'),
		'DM' => trans('setting/country.DM'),
		'DO' => trans('setting/country.DO'),
		'TP' => trans('setting/country.TP'),
		'EC' => trans('setting/country.EC'),
		'EG' => trans('setting/country.EG'),
		'SV' => trans('setting/country.SV'),
		'GQ' => trans('setting/country.GQ'),
		'ER' => trans('setting/country.ER'),
		'EE' => trans('setting/country.EE'),
		'ET' => trans('setting/country.ET'),
		'FK' => trans('setting/country.FK'),
		'FO' => trans('setting/country.FO'),
		'FJ' => trans('setting/country.FJ'),
		'FI' => trans('setting/country.FI'),
		'FR' => trans('setting/country.FR'),
		'FX' => trans('setting/country.FX'),
		'GF' => trans('setting/country.GF'),
		'PF' => trans('setting/country.PF'),
		'TF' => trans('setting/country.TF'),
		'GA' => trans('setting/country.GA'),
		'GM' => trans('setting/country.GM'),
		'GE' => trans('setting/country.GE'),
		'DE' => trans('setting/country.DE'),
		'GH' => trans('setting/country.GH'),
		'GI' => trans('setting/country.GI'),
		'GK' => trans('setting/country.GK'),
		'GR' => trans('setting/country.GR'),
		'GL' => trans('setting/country.GL'),
		'GD' => trans('setting/country.GD'),
		'GP' => trans('setting/country.GP'),
		'GU' => trans('setting/country.GU'),
		'GT' => trans('setting/country.GT'),
		'GN' => trans('setting/country.GN'),
		'GW' => trans('setting/country.GW'),
		'GY' => trans('setting/country.GY'),
		'HT' => trans('setting/country.HT'),
		'HM' => trans('setting/country.HM'),
		'HN' => trans('setting/country.HN'),
		'HK' => trans('setting/country.HK'),
		'HU' => trans('setting/country.HU'),
		'IS' => trans('setting/country.IS'),
		'IN' => trans('setting/country.IN'),
		'IM' => trans('setting/country.IM'),
		'ID' => trans('setting/country.ID'),
		'IR' => trans('setting/country.IR'),
		'IQ' => trans('setting/country.IQ'),
		'IE' => trans('setting/country.IE'),
		'IL' => trans('setting/country.IL'),
		'IT' => trans('setting/country.IT'),
		'CI' => trans('setting/country.CI'),
		'JE' => trans('setting/country.JE'),
		'JM' => trans('setting/country.JM'),
		'JP' => trans('setting/country.JP'),
		'JO' => trans('setting/country.JO'),
		'KZ' => trans('setting/country.KZ'),
		'KE' => trans('setting/country.KE'),
		'KI' => trans('setting/country.KI'),
		'KP' => trans('setting/country.KP'),
		'KR' => trans('setting/country.KR'),
		'XK' => trans('setting/country.XK'),
		'KW' => trans('setting/country.KW'),
		'KG' => trans('setting/country.KG'),
		'LA' => trans('setting/country.LA'),
		'LV' => trans('setting/country.LV'),
		'LB' => trans('setting/country.LB'),
		'LS' => trans('setting/country.LS'),
		'LR' => trans('setting/country.LR'),
		'LY' => trans('setting/country.LY'),
		'LI' => trans('setting/country.LI'),
		'LT' => trans('setting/country.LT'),
		'LU' => trans('setting/country.LU'),
		'MO' => trans('setting/country.MO'),
		'MK' => trans('setting/country.MK'),
		'MG' => trans('setting/country.MG'),
		'MW' => trans('setting/country.MW'),
		'MY' => trans('setting/country.MY'),
		'MV' => trans('setting/country.MV'),
		'ML' => trans('setting/country.ML'),
		'MT' => trans('setting/country.MT'),
		'MH' => trans('setting/country.MH'),
		'MQ' => trans('setting/country.MQ'),
		'MR' => trans('setting/country.MR'),
		'MU' => trans('setting/country.MU'),
		'TY' => trans('setting/country.TY'),
		'MX' => trans('setting/country.MX'),
		'FM' => trans('setting/country.FM'),
		'MD' => trans('setting/country.MD'),
		'MC' => trans('setting/country.MC'),
		'MN' => trans('setting/country.MN'),
		'ME' => trans('setting/country.ME'),
		'MS' => trans('setting/country.MS'),
		'MA' => trans('setting/country.MA'),
		'MZ' => trans('setting/country.MZ'),
		'MM' => trans('setting/country.MM'),
		'NA' => trans('setting/country.NA'),
		'NR' => trans('setting/country.NR'),
		'NP' => trans('setting/country.NP'),
		'NL' => trans('setting/country.NL'),
		'AN' => trans('setting/country.AN'),
		'NC' => trans('setting/country.NC'),
		'NZ' => trans('setting/country.NZ'),
		'NI' => trans('setting/country.NI'),
		'NE' => trans('setting/country.NE'),
		'NG' => trans('setting/country.NG'),
		'NU' => trans('setting/country.NU'),
		'NF' => trans('setting/country.NF'),
		'MP' => trans('setting/country.MP'),
		'NO' => trans('setting/country.NO'),
		'OM' => trans('setting/country.OM'),
		'PK' => trans('setting/country.PK'),
		'PW' => trans('setting/country.PW'),
		'PS' => trans('setting/country.PS'),
		'PA' => trans('setting/country.PA'),
		'PG' => trans('setting/country.PG'),
		'PY' => trans('setting/country.PY'),
		'PE' => trans('setting/country.PE'),
		'PH' => trans('setting/country.PH'),
		'PN' => trans('setting/country.PN'),
		'PL' => trans('setting/country.PL'),
		'PT' => trans('setting/country.PT'),
		'PR' => trans('setting/country.PR'),
		'QA' => trans('setting/country.QA'),
		'RE' => trans('setting/country.RE'),
		'RO' => trans('setting/country.RO'),
		'RU' => trans('setting/country.RU'),
		'RW' => trans('setting/country.RW'),
		'KN' => trans('setting/country.KN'),
		'LC' => trans('setting/country.LC'),
		'VC' => trans('setting/country.VC'),
		'WS' => trans('setting/country.WS'),
		'SM' => trans('setting/country.SM'),
		'ST' => trans('setting/country.ST'),
		'SA' => trans('setting/country.SA'),
		'SN' => trans('setting/country.SN'),
		'RS' => trans('setting/country.RS'),
		'SC' => trans('setting/country.SC'),
		'SL' => trans('setting/country.SL'),
		'SG' => trans('setting/country.SG'),
		'SK' => trans('setting/country.SK'),
		'SI' => trans('setting/country.SI'),
		'SB' => trans('setting/country.SB'),
		'SO' => trans('setting/country.SO'),
		'ZA' => trans('setting/country.ZA'),
		'GS' => trans('setting/country.GS'),
		'ES' => trans('setting/country.ES'),
		'LK' => trans('setting/country.LK'),
		'SH' => trans('setting/country.SH'),
		'PM' => trans('setting/country.PM'),
		'SD' => trans('setting/country.SD'),
		'SR' => trans('setting/country.SR'),
		'SJ' => trans('setting/country.SJ'),
		'SZ' => trans('setting/country.SZ'),
		'SE' => trans('setting/country.SE'),
		'CH' => trans('setting/country.CH'),
		'SY' => trans('setting/country.SY'),
		'TW' => trans('setting/country.TW'),
		'TJ' => trans('setting/country.TJ'),
		'TZ' => trans('setting/country.TZ'),
		'TH' => trans('setting/country.TH'),
		'TG' => trans('setting/country.TG'),
		'TK' => trans('setting/country.TK'),
		'TO' => trans('setting/country.TO'),
		'TT' => trans('setting/country.TT'),
		'TN' => trans('setting/country.TN'),
		'TR' => trans('setting/country.TR'),
		'TM' => trans('setting/country.TM'),
		'TC' => trans('setting/country.TC'),
		'TV' => trans('setting/country.TV'),
		'UG' => trans('setting/country.UG'),
		'UA' => trans('setting/country.UA'),
		'AE' => trans('setting/country.AE'),
		'GB' => trans('setting/country.GB'),
		'US' => trans('setting/country.US'),
		'UM' => trans('setting/country.UM'),
		'UY' => trans('setting/country.UY'),
		'UZ' => trans('setting/country.UZ'),
		'VU' => trans('setting/country.VU'),
		'VA' => trans('setting/country.VA'),
		'VE' => trans('setting/country.VE'),
		'VN' => trans('setting/country.VN'),
		'VG' => trans('setting/country.VG'),
		'VI' => trans('setting/country.VI'),
		'WF' => trans('setting/country.WF'),
		'EH' => trans('setting/country.EH'),
		'YE' => trans('setting/country.YE'),
		'ZR' => trans('setting/country.ZR'),
		'ZM' => trans('setting/country.ZM'),
		'ZW' => trans('setting/country.ZW')
	];
	if($a==null){
		return $output;
	}
	else{
		return $output[$a];
	}
}

function spliterror($validator)
{
	$output =[];
	foreach($validator->messages()->messages() as $message){
        foreach($message as $msg){
            $output[] = $msg;
        }
    }
    return $output;
}

//-------------------------------------------------------------------------------


// start: keystring function function added by rabbi
function arrKeyOnly($array, $seperator=','){
    $string ='';
    $sep = '';
    foreach($array as $key => $val){
        $string .= $sep.$key;
        $sep=$seperator;
    }
    return $string;
}
// end: keystring function

// start: keystring function function added by rabbi
function arrValueOnly($array, $seperator=','){
    $string ='';
    $sep = '';
    foreach($array as $key => $val){
        $string .= $sep.$val;
        $sep=$seperator;
    }
    return $string;
}
// end: keystring function

//Start: Clean String without space
function cleanslug($post=''){
    $post =  preg_replace('/\s\s+/', ' ', $post);
    $post = str_replace(' ', '-', $post); // Replaces all spaces with hyphens.
    $post = preg_replace('/[^A-Za-z0-9\-]/', '', $post); // Removes special chars.
    $post = preg_replace('/-+/', '-', $post);
    $a = substr($post, -1);
    if($a== '-'){
         $post= rtrim($post, '-');
         $post= clean_fields($post);
    }
    return $post;
}


// image path------------------------
function path_favicon(){
 return 'assets/images/favicon/';
}

function path_logo(){
 return 'assets/img/logo/';
}

function path_images(){
 return 'assets/img/';
}

function path_thumbs(){
 	return path_images().'thumbs/';
}

function path_profile(){
 	return path_images().'profiles/';
}

// date fuction
function getDateFormat($date = null, $format = 'd M, Y')
  {
    if($date == null)
	{
		$date = date('d-m-Y');
	}

	return date_format(date_create($date),$format);
  }

function chdate($date){
	$date = explode('-', $date);
	$countval = count($date);
	if($countval==3 && ctype_digit($date[0])==true && ctype_digit($date[1])==true && ctype_digit($date[2])==true && checkdate($date[1], $date[2], $date[0])){
		return true;
	}
	else{
		return false;
	}
}

function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}


// image uploading system added by rabbi
function uploadimage($img,$path,$width=null,$height=null)
{
    // saving image in target path
    $imgName = uniqid() . '.' . $img->getClientOriginalExtension();
    $imgPath = public_path($path.$imgName);

    

    // configure with favored image driver (gd by default)
    // Image::configure(array('driver' => 'imagick'));

    // making image
    $makeImg = Image::make($img);
    if($width!=null && $height!=null && is_int($width) && is_int($height))
    {
        // $makeImg->resize($width, $height);
        $makeImg->fit($width, $height);
    }

    if($makeImg->save($imgPath))
    {
        return $imgName;
    }
    return false;
}
// for deleting uploaded image in server folder added by rabbi
function removeuploadedimage($imgName, $path) 
{
    $imagePath = public_path($path).$imgName;
    $deletedImg = File::delete($imagePath);

    if($deletedImg)
    {
        return true;
    }
    return false;
}

//Random Screen
function randomString($a=10){
    $x = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'; $c=strlen($x)-1; $z='';
    for($i=0; $i<$a; $i++){ $y= rand(0, $c); $z .=substr($x, $y, 1); }
    return $z;
}

// random number
function randomNumber($a=10){
    $x = '0123456789'; $c=strlen($x)-1; $z='';
    for($i=0; $i<$a; $i++){ $y= rand(0, $c); $z .=substr($x, $y, 1); }
    return $z;
}

function colquery($columns, $key='sort', $default='id'){
        $data = \Request::get($key);
        if($data && !empty($data) && array_key_exists($data,$columns)){
            $data = $columns[$data];
        }
        else{
            $data = $default;
        }
        return $data;
    }

function insertgetinput($get){
    	$output = '';
    	if(isset($_GET)){
    		foreach($_GET as $key=>$val){
    			if(in_array($key,$get)==false && $val!=''){
    				$output .= '<input type="hidden" name="'.$key.'" value="'.$val.'">'; 
    			}
    		}
    	}
    	return $output;
    }

function returnget($key, $val=''){
    	$output = '';
    	if(isset($_GET) && isset($_GET[$key]) && $_GET[$key]==$val && $val!=''){
    		$output=' selected';
    	}
    	elseif(isset($_GET) && isset($_GET[$key]) && $val==''){
    		$output= $_GET[$key];
    	}
    	return $output;
    }

function breadcrumb($data, $seperator='angle-right'){
    	$output='';
    	foreach ($data as $val) {
    		$output .= '<li>';
	        $output .= '<i class="icon-'.$val[2].'"></i>';
	        $output .= '<a href="';
	        if(is_array($val[1])){
        		$output .= route($value[0], $value[1]);
	        }
	        else{
	        	$output .= route($val[0]);
	        }
	        $output .= '">'.$val[1].'</a>';
	        $output .= '<i class="fa fa-'.$seperator.'"></i>';
	        $output .= '</li>';
    	}
    	return $output;
    }

function datebreaker($input, $format){
		$a = strtotime($input);
		$a = date($format, $a);
		$a = (integer)$a;
		return $a;
	}
function carbondate($input){
		$input = explode('-', $input);
		if(count($input)>=3){
			$a['y'] = $input[0];
			$a['m'] = $input[1];
			$a['d'] = $input[2];
			return Carbon::create($a['y'], $a['m'], $a['d'], 0, 0, 0, 0);
		}
		return '';
	}
function carbonext($input){
		$input = explode('-', $input);
		if(count($input)>=3){
			$a['y'] = $input[0];
			$a['m'] = $input[1];
			$a['d'] = $input[2];
			return Carbon::create($a['y'], $a['m'], $a['d'], 0, 0, 0, 0)->addDay();
		}
		return '';
	}