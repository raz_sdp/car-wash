<?php

Route::get('/checkin',['uses' =>'Rentcar\CheckinController@checkin', 'as' => 'checkin']);
Route::get('/checkin/{lang}',['uses' =>'Rentcar\CheckinController@checkin', 'as' => 'checkin']);
Route::get('/checkincarlist',['uses' =>'Rentcar\CheckinController@checkincarlist', 'as' => 'checkincarlist']);
Route::get('/checkincarlist/{lang}',['uses' =>'Rentcar\CheckinController@checkincarlist', 'as' => 'checkincarlist']);
Route::get('/singlecheckin/{id}',['uses' =>'Rentcar\CheckinController@singlecheckin', 'as' => 'singlecheckin']);
Route::get('/singlecheckin/{id}/{lang}',['uses' =>'Rentcar\CheckinController@singlecheckin', 'as' => 'singlecheckin']);

Route::post('/getcardetails',['uses' =>'MainController@getcardetails', 'as' => 'getcardetails']);