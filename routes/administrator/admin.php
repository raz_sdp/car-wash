<?php

Route::group(['middleware'=>['admin']], function(){
	Route::group(['prefix' => 'admin'], function () {
		
    Route::get('/addemployee',['uses' =>'Admin\UserController@addemployee', 'as' => 'addemployee']);
		Route::get('/addemployee/{lang}',['uses' =>'Admin\UserController@addemployee', 'as' => 'addemployee'])->where('lang', langexp());
       
    Route::post('/addemployeeprocess',['uses' =>'Admin\UserController@addemployeeprocess', 'as' => 'addemployeeprocess']);
	  Route::post('/addemployeeprocess/{lang}',['uses' =>'Admin\UserController@addemployeeprocess', 'as' => 'addemployeeprocess'])->where('lang', langexp());
       
    Route::post('/editemployeeprocess',['uses' =>'Admin\UserController@editemployeeprocess', 'as' => 'editemployeeprocess']);
    Route::post('/editemployeeprocess/{lang}',['uses' =>'Admin\UserController@editemployeeprocess', 'as' => 'editemployeeprocess'])->where('lang', langexp());
       

    Route::get('/viewallemployee',['uses' =>'Admin\UserController@viewallemployee', 'as' => 'viewallemployee']);
	  Route::get('/viewallemployee/{lang}',['uses' =>'Admin\UserController@viewallemployee', 'as' => 'viewallemployee'])->where('lang', langexp());
       
    //    Route::get('/editemployee/{id}',['uses' =>'Admin\UserController@editemployee', 'as' => 'editemployee'])->where('id', '[0-9]+');
	   // Route::get('/editemployee/{id}/{lang}',['uses' =>'Admin\UserController@editemployee', 'as' => 'editemployee'])->where(['id' => '[0-9]+', 'lang' => langexp()]);
       
    Route::get('/makeuser/{id}',['uses' =>'Admin\UserController@makeuser', 'as' => 'makeuser'])->where('id', '[0-9]+');
	  Route::get('/makeuser/{id}/{lang}',['uses' =>'Admin\UserController@makeuser', 'as' => 'makeuser'])->where(['id' => '[0-9]+', 'lang' => langexp()]);
       
    Route::post('/makeuserprocess',['uses' =>'Admin\UserController@makeuserprocess', 'as' => 'makeuserprocess']);
	  Route::post('/makeuserprocess/{lang}',['uses' =>'Admin\UserController@makeuserprocess', 'as' => 'makeuserprocess'])->where('lang', langexp());
     
    Route::get('/viewalluser',['uses' =>'Admin\UserController@viewalluser', 'as' => 'viewalluser']);
	  Route::get('/viewalluser/{lang}',['uses' =>'Admin\UserController@viewalluser', 'as' => 'viewalluser'])->where('lang', langexp());
       
    Route::get('/employee/{id}',['uses' =>'Admin\UserController@employee', 'as' => 'employee']);
	  Route::get('/employee/{id}/{lang}',['uses' =>'Admin\UserController@employee', 'as' => 'employee'])->where('lang', langexp());
       
       
      Route::get('/regcompany',['uses' =>'Admin\RccompanyController@regcompany', 'as' => 'regcompany']);
      Route::get('/regcompany/{lang}',['uses' =>'Admin\RccompanyController@regcompany', 'as' => 'regcompany'])->where(['lang' => langexp()]);
     
      Route::post('/regcompanyprocess',['uses' =>'Admin\RccompanyController@regcompanyprocess', 'as' => 'regcompanyprocess']);
      Route::post('/regcompanyprocess/{lang}',['uses' =>'Admin\RccompanyController@regcompanyprocess', 'as' => 'regcompanyprocess'])->where(['lang' => langexp()]);

      Route::post('/companyupdateprocess/{id}',['uses' =>'Admin\RccompanyController@companyupdateprocess', 'as' => 'companyupdateprocess']);
      Route::post('/companyupdateprocess/{id}/{lang}',['uses' =>'Admin\RccompanyController@companyupdateprocess', 'as' => 'companyupdateprocess'])->where(['lang' => langexp()]);

      Route::get('/viewallcompany',['uses' =>'Admin\RccompanyController@viewallcompany', 'as' => 'viewallcompany']);
      Route::post('/viewallcompany/{lang}',['uses' =>'Admin\RccompanyController@viewallcompany', 'as' => 'viewallcompany'])->where(['lang' => langexp()]);

      Route::get('/company/{id}',['uses' =>'Admin\RccompanyController@company', 'as' => 'company'])->where('id', '[0-9]+');
      Route::post('/company/{id}/{lang}',['uses' =>'Admin\RccompanyController@company', 'as' => 'company'])->where(['id' => '[0-9]+', 'lang' => langexp()]);
  
      Route::get('/companydelete/{id}',['uses' =>'Admin\RccompanyController@companydelete', 'as' => 'companydelete']);
      Route::get('/companydelete/{id}/{lang}',['uses' =>'Admin\RccompanyController@companydelete', 'as' => 'companydelete'])->where(['lang' => langexp()]);
  
      
     Route::get('/regcar',['uses' =>'Admin\RccarController@regcar', 'as' => 'regcar']);
     Route::get('/regcar/{lang}',['uses' =>'Admin\RccarController@regcar', 'as' => 'regcar'])->where('lang', langexp());
    
     Route::post('/regcarprocess',['uses' =>'Admin\RccarController@regcarprocess', 'as' => 'regcarprocess']);
     Route::post('/regcarprocess/{lang}',['uses' =>'Admin\RccarController@regcarprocess', 'as' => 'regcarprocess'])->where('lang', langexp());
  
     Route::get('/viewallcar',['uses' =>'Admin\RccarController@viewallcar', 'as' => 'viewallcar']);
     Route::post('/viewallcar/{lang}',['uses' =>'Admin\RccarController@viewallcar', 'as' => 'viewallcar'])->where(['lang' => langexp()]);
  
     
  });

});