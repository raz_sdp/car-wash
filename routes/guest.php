<?php

Route::group(['middleware'=>['guest']], function(){

  Route::get('/login',['uses' =>'Authcontroller@login', 'as' => 'login']);
  Route::get('/login/{lang}',['uses' =>'Authcontroller@login', 'as' => 'login'])->where('lang', langexp());;
  Route::post('/loginprocess',['uses' =>'AuthController@loginprocess', 'as' => 'loginprocess']);
  Route::post('/loginprocess/{lang}',['uses' =>'AuthController@loginprocess', 'as' => 'loginprocess'])->where('lang', langexp());


});
