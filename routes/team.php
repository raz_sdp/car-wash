<?php

Route::group(['middleware'=>['team']], function(){

  Route::get('/dashboard',['uses' =>'Maincontroller@home', 'as' => 'dashboard']);
  Route::get('/dashboard/{lang}',['uses' =>'Maincontroller@home', 'as' => 'dashboard'])->where('lang', langexp());;
 
  Route::get('/logout', ['uses' => 'AuthController@logout', 'as' => 'logout']);
  Route::get('/logout/{lang}', ['uses' => 'AuthController@logout', 'as' => 'logout'])->where('lang', langexp());

});