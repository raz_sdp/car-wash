<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('layoutmaster');
// });
require base_path('routes/administrator/admin.php');
require base_path('routes/rentcar/checkin.php');
require base_path('routes/guest.php');
require base_path('routes/team.php');


Route::get('/import',['uses' =>'importController@import', 'as' => 'import']);
Route::get('/import/{lang}',['uses' =>'ImportController@import', 'as' => 'import'])->where('lang', langexp());

Route::post('/importprocess/{model}',['uses' =>'ImportController@importprocess', 'as' => 'importprocess']);
Route::get('/importprocess/{model}/{lang}',['uses' =>'ImportController@importprocess', 'as' => 'importprocess'])->where('lang', langexp());