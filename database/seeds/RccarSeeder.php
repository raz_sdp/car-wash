<?php

use Illuminate\Database\Seeder;
use App\Models\rc\Rccar;
class RccarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$rccar = new Rccar();
		$rccar->cbarcode = '12345678901234567890';
		$rccar->rccompany_id = 1;
		$rccar->save();
    }
}
