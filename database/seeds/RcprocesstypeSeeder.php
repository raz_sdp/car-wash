<?php

use Illuminate\Database\Seeder;
use App\Models\rc\Rcprocesstype;
class RcprocesstypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rcprocesstype = new Rcprocesstype();
        $rcprocesstype->name = 'washing';
        $rcprocesstype->save();
    }
}
