<?php
use Illuminate\Database\Seeder;
use App\Models\rc\Rcuser;
class RcuserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rcuser = new Rcuser();
		$rcuser->role = 1;
		$rcuser->password = Hash::make('123456');
		$rcuser->acstatus = 1;
		$rcuser->email = "admin@gmail.com";
		$rcuser->ubarcode = "GR13";
		$rcuser->assignedby = 1;
		$rcuser->rcemployee_id = 1;
        $rcuser->save();

        $rcuser = new Rcuser();
        $rcuser->role = 2;
        $rcuser->password = Hash::make('123456');
        $rcuser->acstatus = 1;
        $rcuser->email = "supervisor@gmail.com";
        $rcuser->ubarcode = "GR10";
        $rcuser->assignedby = 1;
        $rcuser->rcemployee_id = 2;
        $rcuser->save();

        $rcuser = new Rcuser();
        $rcuser->role = 3;
        $rcuser->password = Hash::make('123456');
        $rcuser->acstatus = 1;
        $rcuser->email = "oparetor@gmail.com";
        $rcuser->ubarcode = "GR03";
        $rcuser->assignedby = 1;
        $rcuser->rcemployee_id = 3;
        $rcuser->save();

    }
}
