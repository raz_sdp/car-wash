<?php

use Illuminate\Database\Seeder;
use App\Models\rc\Rcsubprocess;
class RcsubprocessSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rcsubprocess = new Rcsubprocess();
		$rcsubprocess->rcprocesstype_id = 1;
		$rcsubprocess->name = 'washing';
		$rcsubprocess->save();
    }
}
