<?php

use Illuminate\Database\Seeder;
use App\Models\rc\Rcprocess;
class RcprocessSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rcprocess = new Rcprocess();
		$rcprocess->rccar_id = 1;
		$rcprocess->rcprocesstype_id = 1;
		$rcprocess->assignedby = 1;
		$rcprocess->in ='03:08:08';
		$rcprocess->out = '06:08:08';
		$rcprocess->status = 1;
		$rcprocess->rcuser_id = 1;
		$rcprocess->ubarcode = '12345678901234567892';
		$rcprocess->currentprocess = 1;
        $rcprocess->save();
    }
}
