<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $this->call(RcdepartmentSeeder::class);
       $this->call(RcemployeeSeeder::class);
       $this->call(RcuserSeeder::class);
       $this->call(RcattendanceSeeder::class);
       $this->call(RccompanySeeder::class);
       $this->call(RccarSeeder::class);
       $this->call(RcdutySeeder::class);
       $this->call(RcshiftSeeder::class);
       $this->call(RcdailytaskSeeder::class);
       $this->call(RcprocesstypeSeeder::class);
       $this->call(RcprocessSeeder::class);
       $this->call(RcsubprocessSeeder::class);
        $this->call(AdminsettingSeeder::class);
    }
}
