<?php

use Illuminate\Database\Seeder;
use App\Models\Adminsetting;

class AdminsettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        $adminsetting = new Adminsetting();
        $adminsetting->slug = 'lang';
        $adminsetting->value = 'en';
        $adminsetting->save();

        $adminsetting = new Adminsetting();
        $adminsetting->slug = 'item_perpage';
        $adminsetting->value = '10';
        $adminsetting->save();

        $adminsetting = new Adminsetting();
        $adminsetting->slug = 'moneysymbol';
        $adminsetting->value = '৳';
        $adminsetting->save();

        $adminsetting = new Adminsetting();
        $adminsetting->slug = 'breadcrumb_seperator';
        $adminsetting->value = 'angle-right';
        $adminsetting->save();

        $adminsetting = new Adminsetting();
        $adminsetting->slug = 'global_commission';
        $adminsetting->value = '1';
        $adminsetting->save();

        $adminsetting = new Adminsetting();
        $adminsetting->slug = 'sponsor';
        $adminsetting->value = '10';
        $adminsetting->save();
        
        $adminsetting = new Adminsetting();
        $adminsetting->slug = 'ref1';
        $adminsetting->value = '4';
        $adminsetting->save();
        
        $adminsetting = new Adminsetting();
        $adminsetting->slug = 'ref2';
        $adminsetting->value = '4';
        $adminsetting->save();
        
        $adminsetting = new Adminsetting();
        $adminsetting->slug = 'ref3';
        $adminsetting->value = '4';
        $adminsetting->save();
        
        $adminsetting = new Adminsetting();
        $adminsetting->slug = 'ref4';
        $adminsetting->value = '4';
        $adminsetting->save();
        
        $adminsetting = new Adminsetting();
        $adminsetting->slug = 'ref5';
        $adminsetting->value = '4';
        $adminsetting->save();
        
        $adminsetting = new Adminsetting();
        $adminsetting->slug = 'ref6';
        $adminsetting->value = '4';
        $adminsetting->save();
        
        $adminsetting = new Adminsetting();
        $adminsetting->slug = 'ref7';
        $adminsetting->value = '4';
        $adminsetting->save();
        
        $adminsetting = new Adminsetting();
        $adminsetting->slug = 'ref8';
        $adminsetting->value = '4';
        $adminsetting->save();
        
        $adminsetting = new Adminsetting();
        $adminsetting->slug = 'ref9';
        $adminsetting->value = '4';
        $adminsetting->save();

        $adminsetting = new Adminsetting();
        $adminsetting->slug = 'ref10';
        $adminsetting->value = '4';
        $adminsetting->save();

        $adminsetting = new Adminsetting();
        $adminsetting->slug = 'withdrawal_fees';
        $adminsetting->value = '1';
        $adminsetting->save();

        $adminsetting = new Adminsetting();
        $adminsetting->slug = 'vat';
        $adminsetting->value = '5';
        $adminsetting->save();

        $adminsetting = new Adminsetting();
        $adminsetting->slug = 'min_withdrawal';
        $adminsetting->value = '1000';
        $adminsetting->save();

        $adminsetting = new Adminsetting();
        $adminsetting->slug = 'product_extra_price';
        $adminsetting->value = '5';
        $adminsetting->save();

        $adminsetting = new Adminsetting();
        $adminsetting->slug = 'adminemail';
        $adminsetting->value = 'zahidfreelancer@gmail.com';
        $adminsetting->save();

        $adminsetting = new Adminsetting();
        $adminsetting->slug = 'mail_driver';
        $adminsetting->value = 'smtp';
        $adminsetting->save();

        $adminsetting = new Adminsetting();
        $adminsetting->slug = 'mail_host';
        $adminsetting->value = 'smtp.gmail.com';
        $adminsetting->save();

        $adminsetting = new Adminsetting();
        $adminsetting->slug = 'mail_port';
        $adminsetting->value = '587';
        $adminsetting->save();

        $adminsetting = new Adminsetting();
        $adminsetting->slug = 'mail_username';
        $adminsetting->value = 'golamrabbi3@gmail.com';
        $adminsetting->save();

        $adminsetting = new Adminsetting();
        $adminsetting->slug = 'mail_password';
        $adminsetting->value = 'jqubswquoozhgjro';
        $adminsetting->save();

        $adminsetting = new Adminsetting();
        $adminsetting->slug = 'mail_encryption';
        $adminsetting->value = 'tls';
        $adminsetting->save();

        $adminsetting = new Adminsetting();
        $adminsetting->slug = 'mail_from_address';
        $adminsetting->value = 'info@el-earn.com';
        $adminsetting->save();

        $adminsetting = new Adminsetting();
        $adminsetting->slug = 'mail_from_name';
        $adminsetting->value = 'El-Earn';
        $adminsetting->save();
    }
}
