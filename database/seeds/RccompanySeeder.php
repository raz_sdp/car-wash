<?php

use Illuminate\Database\Seeder;
use App\Models\rc\Rccompany;
class RccompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rccompany = new Rccompany();
		$rccompany->name = 'company name';
        $rccompany->country = 'bd';
        $rccompany->email = 'company@mail.com';
        $rccompany->state = 'khulna';
        $rccompany->streetone = 'street name one';
        $rccompany->streettwo = 'street name two';
		$rccompany->postcode = '9100';
		$rccompany->website = 'https:demoweb.com';
		$rccompany->phonenumber = '+8801234567891';
        $rccompany->save();
    }
}
