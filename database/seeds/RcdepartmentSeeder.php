<?php

use Illuminate\Database\Seeder;
use App\Models\rc\Rcdepartment;

class RcdepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rcdepartment = new Rcdepartment();
        $rcdepartment->name = 'Checking';
        $rcdepartment->save();
        
        $rcdepartment = new Rcdepartment();
        $rcdepartment->name = 'Washing';
        $rcdepartment->save();   


        $rcdepartment = new Rcdepartment();
        $rcdepartment->name = 'Repair';
        $rcdepartment->save();

        $rcdepartment = new Rcdepartment();
        $rcdepartment->name = 'Refuel';
        $rcdepartment->save();
    }
}
