<?php

use Illuminate\Database\Seeder;
use App\Models\rc\Rcemployee;

class RcemployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rcecemployee = new Rcemployee();
        $rcecemployee->ubarcode = 'GR13';
        $rcecemployee->fname = 'Mr';
        $rcecemployee->lname = 'admin';
        $rcecemployee->address = 'khulna';
        $rcecemployee->country = 'BD';
        $rcecemployee->dob = '1978-06-13';
        $rcecemployee->phonenumber = '+88012345678912';
        $rcecemployee->gender = 1;
        $rcecemployee->email = 'admin@gmail.com';
        $rcecemployee->rcdepartment_id = 1;
        $rcecemployee->joindate = '2016-01-01';
        $rcecemployee->save();

        $rcecemployee = new Rcemployee();
        $rcecemployee->ubarcode = 'GR10';
        $rcecemployee->fname = 'Mr';
        $rcecemployee->lname = 'supervisor';
        $rcecemployee->address = 'khulna';
        $rcecemployee->country = 'BD';
        $rcecemployee->dob = '1978-06-13';
        $rcecemployee->phonenumber = '+88012345678912';
        $rcecemployee->gender = 1;
        $rcecemployee->email = 'supervisor@gmail.com';
        $rcecemployee->rcdepartment_id = 1;
        $rcecemployee->joindate = '2016-01-01';
        $rcecemployee->save();

        $rcecemployee = new Rcemployee();
        $rcecemployee->ubarcode = 'GR03';
        $rcecemployee->fname = 'Mr';
        $rcecemployee->lname = 'oparetor';
        $rcecemployee->address = 'khulna';
        $rcecemployee->country = 'BD';
        $rcecemployee->dob = '1978-06-13';
        $rcecemployee->phonenumber = '+88012345678912';
        $rcecemployee->gender = 1;
        $rcecemployee->email = 'oparetor@gmail.com';
        $rcecemployee->rcdepartment_id = 1;
        $rcecemployee->joindate = '2016-01-01';
        $rcecemployee->save();
    }
}
