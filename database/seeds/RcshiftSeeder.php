<?php

use Illuminate\Database\Seeder;
use App\Models\rc\Rcshift;
class RcshiftSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rcshift = new Rcshift();
		$rcshift->name = 'dayshift';
		$rcshift->assignedby = 1;
		$rcshift->start = '2017-06-13 03:06:05';
		$rcshift->end = '2017-06-13 09:06:05';
        $rcshift->save();
    }
}
