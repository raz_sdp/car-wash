<?php

use Illuminate\Database\Seeder;
use App\Models\rc\Rcdailytask;
class RcdailytaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rcdailytask = new Rcdailytask();
		$rcdailytask->rcduty_id = 1;
		$rcdailytask->rcshif_id = 1;
		$rcdailytask->assignedby = 1;
		$rcdailytask->rcemployee_id = 2;
		$rcdailytask->ubarcode = '12345678901234567891';
        $rcdailytask->save();
    }
}
