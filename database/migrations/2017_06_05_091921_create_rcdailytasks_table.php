<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRcdailytasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rcdailytasks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('rcduty_id')->unsigned();
            $table->foreign('rcduty_id')->references('id')->on('rcduties')->onDelete('cascade')->onUpdate('cascade');
            $table->bigInteger('rcshif_id')->unsigned();
            $table->foreign('rcshif_id')->references('id')->on('rcshifts')->onDelete('cascade')->onUpdate('cascade');
            $table->bigInteger('assignedby')->unsigned();
            $table->foreign('assignedby')->references('id')->on('rcusers')->onDelete('cascade')->onUpdate('cascade');
            $table->bigInteger('rcemployee_id')->unsigned();
            $table->foreign('rcemployee_id')->references('id')->on('rcemployees')->onDelete('cascade')->onUpdate('cascade');
            $table->string('ubarcode');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rcdailytasks');
    }
}
