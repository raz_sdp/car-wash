<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRccarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rccars', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('cbarcode')->unique();
            $table->bigInteger('rccompany_id')->unsigned();
            $table->foreign('rccompany_id')->references('id')->on('rccompanies')->onDelete('cascade')->onUpdate('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rccars');
    }
}
