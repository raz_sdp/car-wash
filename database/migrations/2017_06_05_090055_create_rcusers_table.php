<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRcusersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rcusers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('role');
            $table->string('password');
            $table->tinyInteger('acstatus')->default(1);
            $table->string('email')->unique();
            $table->string('ubarcode')->unique();
            $table->bigInteger('assignedby');
            $table->bigInteger('rcemployee_id')->unsigned();
            $table->foreign('rcemployee_id')->references('id')->on('rcemployees')->onDelete('cascade')->onUpdate('cascade');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rcusers');
    }
}
