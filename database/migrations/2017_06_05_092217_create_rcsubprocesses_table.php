<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRcsubprocessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rcsubprocesses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('rcprocesstype_id')->unsigned();
            $table->foreign('rcprocesstype_id')->references('id')->on('rcprocesstypes')->onDelete('cascade')->onUpdate('cascade');
            $table->string('name');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rcsubprocesses');
    }
}
