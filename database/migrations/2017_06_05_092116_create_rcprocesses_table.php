<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRcprocessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rcprocesses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('rccar_id')->unsigned();
            $table->foreign('rccar_id')->references('id')->on('rccars')->onDelete('cascade')->onUpdate('cascade');
            $table->bigInteger('rcprocesstype_id')->unsigned();
            $table->foreign('rcprocesstype_id')->references('id')->on('rcprocesstypes')->onDelete('cascade')->onUpdate('cascade');
            $table->bigInteger('assignedby')->unsigned();
            $table->foreign('assignedby')->references('id')->on('rcusers')->onDelete('cascade')->onUpdate('cascade');
            $table->time('in');
            $table->time('out');
            $table->tinyInteger('status');
            $table->bigInteger('rcuser_id')->unsigned();
            $table->foreign('rcuser_id')->references('id')->on('rcusers')->onDelete('cascade')->onUpdate('cascade');
            $table->string('ubarcode');
            $table->tinyInteger('currentprocess');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rcprocesses');
    }
}
