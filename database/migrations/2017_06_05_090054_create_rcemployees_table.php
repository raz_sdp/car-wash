z<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRcemployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rcemployees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ubarcode')->unique();
            $table->string('fname');
            $table->string('lname');
            $table->string('address');
            $table->string('country');
            $table->date('dob');
            $table->string('phonenumber');
            $table->tinyInteger('gender');
            $table->string('email')->unique();
            $table->bigInteger('rcdepartment_id')->unsigned();
            $table->foreign('rcdepartment_id')->references('id')->on('rcdepartments')->onDelete('cascade')->onUpdate('cascade');
            $table->date('joindate');
            $table->string('avatar')->nullable();
            $table->string('about')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rcemployees');
    }
}
