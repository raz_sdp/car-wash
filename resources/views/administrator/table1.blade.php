
@extends('layoutmaster')
@section('afterstyle')
{{Html::style('assets/vendors/jquery-file-upload/css/jquery.fileupload.css')}}
<style>
    
table.dc-table{border-bottom: none !important;}
table.dc-table .dc-action > div{right:7px;}
.page-header-inner{background: #364150;}
.inline-select{display: inline-block; width: auto}
.dc-select{-webkit-appearance: none;-moz-appearance: none;border-radius: 0;padding-right: 22px;padding-left: 3px;color:#333;background-image: url('img/dropdown.png');background-repeat: no-repeat;background-position: right 5px center;cursor: pointer;color:#999;max-width: 103px;}
.dc-select::-ms-expand {display: none;}
.dc-date{width:88px !important;float:none !important;text-align: center;}
.dc-srchform{position: relative; width:243px;}
.dc-form{padding-bottom: 15px;}
.dc-search{padding-right:52px !important;}
.dc-srcgo {border:1px solid #c2cad8;height: 34px;position: absolute;right: 0;top: 0px;width: 40px;background: #fbf7ff !important;color:#ccc;}
.dc-submit {border:1px solid #c2cad8;height: 34px;right: 0;top: 0px;width: 40px;vertical-align: top;background: #fbf7ff !important;color:#ccc;}
.dc-button-fixer{margin-top:1px; margin-left:3px; }
.portlet.light.bordered {padding-top: 20px;}
.dc-filter{width: 280px; display: inline-block; vertical-align: top;}
table.dataTable > tbody > tr.child ul li {display: inline-block;min-width: 33.33%;padding-right: 16px;}
.dc-totitem{text-align: right; margin-top: 10px;}
@media all and (max-width:767px){.dc-totitem{text-align:left;}}
</style>

@endsection
@section('mainbody')

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase">User List</span>
                </div>
                <div class="tools">
                <div class="btn-group">
                                    <a href="{{route('addemployee',['lang'=>$lang])}}" id="sample_editable_1_new" class="btn sbold green"> Add New
                                        <i class="fa fa-plus"></i>
                                    </a>
                                </div>
                 </div>

            </div>
            <div class="portlet-body">
                   <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="btn-group"> 
                                        <div class="dc-filter">
                                            <form action="" class="dc-form">
                                                <select class="form-control inline-select dc-select" name="sort">
                                                    <option value="">Sort by</option>
                                                    <option value="fn"{{returnget('sort','id')}}>Id</option>
                                                    <option value="em"{{returnget('sort','em')}}>Email</option>
                                                    <option value="r"{{returnget('sort','r')}}>Role</option>
                                                    
                                                </select>
                                                <select class="form-control inline-select dc-select" name="ord">
                                                    <option value="">Order by</option>
                                                    <option value="a"{{returnget('ord','a')}}>Asc</option>
                                                    <option value="d"{{returnget('ord','d')}}>Desc</option>
                                                </select>
                                                {!!insertgetinput(['sort', 'ord'])!!}
                                                <button type="submit" class="btn dc-submit"><i class="fa fa-arrow-right"></i></button>
                                            </form>
                                        </div>
                                        <div class="dc-filter">
                                            <form action="" class="dc-form">
                                                <div class="input-group input-large date-picker input-daterange" data-date="10/11/2012" data-date-format="mm/dd/yyyy">
                                                    <input type="text" class="form-control datepicker dc-date" name="frm" placeholder="From date" value="{{returnget('frm')}}">
                                                    <span class=""> to </span>
                                                    <input type="text" class="form-control datepicker dc-date" name="to" placeholder="To date" value="{{returnget('to')}}">
                                                    {!!insertgetinput(['frm', 'to'])!!} 
                                                    <button type="submit" class="btn dc-submit dc-button-fixer"><i class="fa fa-arrow-right"></i></button>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="dc-filter">
                                            <form action="" class="dc-srchform dc-form">
                                                <input type="text" class="form-control dc-search" name="srch" placeholder="search" value="{{returnget('srch')}}">
                                                {!!insertgetinput(['srch'])!!}
                                                <button type="submit" class="btn dc-srcgo"><i class="fa fa-search"></i></button>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="btn-group pull-right">
                                        <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                            <i class="fa fa-angle-down"></i>
                                        </button>
                                        <ul class="dropdown-menu pull-right">
                                            <li>
                                                <a href="javascript:;">
                                                    <i class="fa fa-print"></i> Print </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                    </div>
                <table class="table table-striped table-bordered table-hover dt-responsive dc-table" width="100%">
                    <thead>
                        <tr>
                            <th class="all">First name</th>
                            <th class="min-phone-l">Last name</th>
                            <th class="min-tablet">Email</th>
                            <th class="none">Mobile</th>
                            <th class="none">Department</th>
                            <th class="desktop">Join date</th>
                            <th class="desktop">Role</th>
                            <th class="all">Extn.</th>
                        </tr>
                    </thead>
                    <tbody>
                       @if(isset($users) && (count($users) > 0))
                           @foreach($users as $user)
                        <tr>
                           <td><a href="{{ route('employee',['id'=>$user->rcemployee_id, 'lang'=>$lang]) }}"> {{ $user->rcemployee->fname }}  </a></td>
                            <td> {{ $user->rcemployee->lname }} </td>
                            <td>
                                 {{ $user->email }} 
                            </td>
                            <td>
                                {{ $user->rcemployee->phonenumber }} 
                            </td>
                            <td>
                              @if(isset($user->rcemployee->rcdepartment) && (count($user->rcemployee->rcdepartment) > 0))
                                  {{ $user->rcemployee->rcdepartment->name }} 
                              @endif  
                            </td>
                            <td class="center"> {{ getDateFormat($user->rcemployee->joindate) }}  </td>
                            <td>
                                {{ role($user->role) }} 
                            </td>
                            <td>
                                <div class="btn-group pull-right">
                                    <button class="btn green btn-xs btn-outline dropdown-toggle" data-toggle="dropdown">Actions
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-right">
                                        <li>
                                            <a href="{{ route('employee',['id'=>$user->rcemployee->id, 'lang'=>$lang]).'#tab_1_3' }}">
                                                <i class="fa fa-pencil"></i> Edit </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;">
                                                <i class="icon-tag"></i> Delete </a>
                                        </li>
                                       
                                    </ul>
                                </div>
                            </td>
                        </tr>
                       @endforeach  
                    @endif 
                    </tbody>
                </table>
                <div class="row">
                <div class="col-sm-6">{{$users->links()}}</div>
                <div class="col-sm-6">
                    <div class="dc-totitem">showing <strong>{{($users->currentPage()-1)*$allsetting['item_perpage']+1}}-{{($users->currentPage()-1)*$allsetting['item_perpage']+$users->count()}}</strong> of <strong>{{$users->total()}}</strong> Users</div>
                </div>
            </div>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
  
</div>
@endsection

@section('afterscripts')
<script type="text/javascript">
    $("#upload").hide();
        $('#fileinput').on('change', function() {
        $("#upload").show();
     
    });
    //Init jquery Date Picker
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        orientation: 'bottom'
    });
 </script>
@endsection