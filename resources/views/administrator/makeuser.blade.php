 @extends('layoutmaster')

@section('mainbody')

<div class="row">
   <div class="col-md-10 col-md-offset-1">
     <div class="portlet light">
         <div class="portlet-title">
            <div class="caption font-blue-sharp">
                <i class="icon-plus font-blue-sunglo"></i>
                <span class="caption-subject bold uppercase">Register User</span>
            </div>
          </div>
          <div class="portlet-body form">
           {{ Form::open(['route' => ['makeuserprocess', 'lang'=>$lang]]) }}
	        <form role="form">
	            <div class="form-body">
	                <input type="hidden" name="id" value="{{$id}}"> 
                  
	                <div class="form-group">
	                    <div class="row">
   							        <div class="col-md-8">
   							            <label>Email Address</label>
	                    			<div class="input-group">
	                       			    <span class="input-group-addon">
	                            		    <i class="fa fa-envelope"></i>
	                        		    </span>
	                                <input type="text" name="email" class="form-control" placeholder="Email Address"> 
	                          </div>
   							        </div>
   							      </div>	
	                </div>
	               	<div class="form-group">
                      <div class="row">
                        <div class="col-md-8">
                            <label class="control-label">User Role</label>
                            <select class="form-control" name="role">
                                    @foreach(role() as $input => $val)
                                      <option value="{{$input}}" <?php if(old('role')!=null && old('role')==$input){echo ' selected';}?>>{{$val}}
                                      </option>
                                    @endforeach
                            </select>
                        </div>
                     </div>
                  </div>  
                  <div class="form-group">
                      <div class="row">
                        <div class="col-md-8">
                           <label class="control-label">Password</label> 
                             <input type="password" name="password"  class="form-control" value="">
                        </div>
                     </div>
                  </div>                                       
	                <div class="form-actions">
    	                <button type="submit" class="btn blue">Submit</button>
    	                <button type="button" class="btn default">Cancel</button>
	               </div>
             {{ Form::close() }}        
	        </form>
        </div>
      </div>
   </div>
</div>

@endsection
