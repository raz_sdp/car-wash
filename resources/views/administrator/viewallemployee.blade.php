@extends('layoutmaster')
@section('afterstyle')
{{Html::style('assets/vendors/jquery-file-upload/css/jquery.fileupload.css')}}
<style>
table.dc-table{border-bottom: none !important;}
table.dc-table .dc-action > div{right:7px;}
.page-header-inner{background: #364150;}
.inline-select{display: inline-block; width: auto}
.dc-select{-webkit-appearance: none;-moz-appearance: none;border-radius: 0;padding-right: 22px;padding-left: 3px;color:#333;background-image: url('img/dropdown.png');background-repeat: no-repeat;background-position: right 5px center;cursor: pointer;color:#999;max-width: 103px;}
.dc-select::-ms-expand {display: none;}
.dc-date{width:88px !important;float:none !important;text-align: center;}
.dc-srchform{position: relative; width:243px;}
.dc-form{padding-bottom: 15px;}
.dc-search{padding-right:52px !important;}
.dc-srcgo {border:1px solid #c2cad8;height: 34px;position: absolute;right: 0;top: 0px;width: 40px;background: #fbf7ff !important;color:#ccc;}
.dc-submit {border:1px solid #c2cad8;height: 34px;right: 0;top: 0px;width: 40px;vertical-align: top;background: #fbf7ff !important;color:#ccc;}
.dc-button-fixer{margin-top:1px; margin-left:3px; }
.portlet.light.bordered {padding-top: 20px;}
.dc-filter{width: 280px; display: inline-block; vertical-align: top;}
table.dataTable > tbody > tr.child ul li {display: inline-block;min-width: 33.33%;padding-right: 16px;}
.dc-totitem{text-align: right; margin-top: 10px;}
@media all and (max-width:767px){.dc-totitem{text-align:left;}}
</style>

@endsection
@section('mainbody')

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase">Employee List</span>
                </div>
                <div class="tools"> 
                   <div class="btn-group">
                        <a href="{{route('addemployee',['lang'=>$lang])}}" id="sample_editable_1_new" class="btn sbold green"> Add New
                            <i class="fa fa-plus"></i>
                        </a>
                    </div>
                    <div class="btn-group">
                      {{ Form::open(array('route'=>['importprocess', 'lang'=>$lang, 'model'=>'user'], 'method'=>'post','files'=>true)) }}
                        <span class="btn green fileinput-button">
                                <i class="fa fa-plus"></i>
                                <span> Import Xl files... </span>
                                <input type="file" id="fileinput" name="files">
                        </span>
                        <button type="submit" id="upload" class="btn blue ">
                                <i class="fa fa-upload"></i>
                                <span> Start upload </span>
                        </button>
   
                      {{ Form::close() }}
                    </div>
                    <div class="btn-group">
                        <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                            <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a href="javascript:;">
                                    <i class="fa fa-print"></i> Print </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                   <div class="table-toolbar">
                        <div class="row">
                                <div class="col-md-10">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="dc-filter">
                                                <form action="" class="dc-form">
                                                    <select class="form-control inline-select dc-select" name="sort">
                                                        <option value="">Sort by</option>
                                                        <option value="i"{{returnget('sort','i')}}>Id</option>
                                                        <option value="fn"{{returnget('sort','fn')}}>First name</option>
                                                        <option value="ln"{{returnget('sort','ln')}}>Last name</option>
                                                        <option value="em"{{returnget('sort','em')}}>Email</option>
                                                        <option value="mb"{{returnget('sort','mb')}}>Mobile</option>
                                                        <option value="j"{{returnget('sort','j')}}>Join date</option>
                                                    </select>
                                                    <select class="form-control inline-select dc-select" name="ord">
                                                        <option value="">Order by</option>
                                                        <option value="a"{{returnget('ord','a')}}>Asc</option>
                                                        <option value="d"{{returnget('ord','d')}}>Desc</option>
                                                    </select>
                                                    {!!insertgetinput(['sort', 'ord'])!!}
                                                    <button type="submit" class="btn dc-submit"><i class="fa fa-arrow-right"></i></button>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="dc-filter">
                                                <form action="" class="dc-form">
                                                    <div class="input-group input-large date-picker input-daterange" data-date="10/11/2012" data-date-format="mm/dd/yyyy">
                                                        <input type="text" class="form-control datepicker dc-date" name="frm" placeholder="From date" value="{{returnget('frm')}}">
                                                        <span class=""> to </span>
                                                        <input type="text" class="form-control datepicker dc-date" name="to" placeholder="To date" value="{{returnget('to')}}">
                                                        {!!insertgetinput(['frm', 'to'])!!} 
                                                        <button type="submit" class="btn dc-submit dc-button-fixer"><i class="fa fa-arrow-right"></i></button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="dc-filter">
                                                <form action="" class="dc-srchform dc-form">
                                                    <input type="text" class="form-control dc-search" name="srch" placeholder="search" value="{{returnget('srch')}}">
                                                    {!!insertgetinput(['srch'])!!}
                                                    <button type="submit" class="btn dc-srcgo"><i class="fa fa-search"></i></button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    
                            </div>
                           
                        </div>        
                       

                    </div>
                <table class="table table-striped table-bordered table-hover dt-responsive  dc-table" width="100%" >
                    <thead>
                        <tr>
                            <th class="all">Id</th>
                            <th class="min-phone-l">Name</th>
                            <th class="min-tablet">Email</th>
                            <th class="none">Mobile</th>
                            <th class="none">Department</th>
                            <th class="desktop">Join date</th>
                            <th class="desktop">Status</th>
                            <th class="all">Extn.</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($employees) && (count($employees) > 0))
                            @foreach($employees as $employee)
                        <tr>
                           <td> {{ $employee->id }} </td>
                            <td>  <a href="{{ route('employee',['id'=>$employee->id, 'lang'=>$lang]) }}">{{ $employee->fname }} {{ $employee->lname }}  </a></td>
                            <td>
                                 {{ $employee->email }} 
                            </td>
                            <td>
                                 {{ $employee-> phonenumber }} 
                            </td>
                            <td>
                              @if(isset($employee->rcdepartment) && (count($employee->rcdepartment) > 0))
                                    {{ $employee->rcdepartment->name }} 
                              @endif  
                            </td>
                            <td class="center"> {{ getDateFormat($employee->joindate) }}  </td>
                            <td class="text-center">
                             @if(isset($employee->rcuser) && (count($employee->rcuser) > 0))
                                <span class="label label-sm label-success"> User </span>
                              @else
                                <span class="label label-sm label-info"> Employee </span>
                              @endif

                            </td>
                            <td>
                                <div class="btn-group pull-right">
                                    <button class="btn green btn-xs btn-outline dropdown-toggle" data-toggle="dropdown">Actions
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-right">
                                        <li>
                                            <a href="{{ route('employee',['id'=>$employee->id, 'lang'=>$lang]) }}">
                                                <i class="fa fa-search"></i> View </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('employee',['id'=>$employee->id, 'lang'=>$lang]).'#tab_1_3' }}">
                                                <i class="fa fa-pencil"></i> Edit </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;">
                                                <i class="icon-tag"></i> Delete </a>
                                        </li>
                                        <li class="divider"> </li>
                                        @if(!isset($employee->rcuser) && (count($employee->rcuser) <= 0) )
                                        <li>
                                            <a href="{{ route('makeuser',['id'=>$employee->id, 'lang'=>$lang]) }}">
                                                <i class="icon-flag"></i> Make User
                                            </a>
                                        </li>
                                        @endif
                                    </ul>
                                </div>
                            </td>
                        </tr>
                       @endforeach  
                    @endif 
                    </tbody>
                </table>
                <div class="row">
                    <div class="col-sm-6">{{$employees->links()}}</div>
                    <div class="col-sm-6">
                        <div class="dc-totitem">showing <strong>{{($employees->currentPage()-1)*$allsetting['item_perpage']+1}}-{{($employees->currentPage()-1)*$allsetting['item_perpage']+$employees->count()}}</strong> of <strong>{{$employees->total()}}</strong> Users</div>
                    </div>
                </div>
                
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
  
</div>
@endsection

@section('afterscripts')
   {{Html::script('assets/vendors/datepicker/datepicker.js')}}
<script type="text/javascript">
    $("#upload").hide();
      $('#fileinput').on('change', function() {
      $("#upload").show();
     
    });
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        orientation: 'bottom'
    });  
 </script>
@endsection