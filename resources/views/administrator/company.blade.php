@extends('layoutmaster')
@section('mainbody')
<div class="row">
 <div class="col-md-12">        
   <!-- BEGIN PAGE TITLE-->
   <h1 class="page-title"> Company Profile 
    <small>view</small>
</h1>
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<div class="profile">
    <div class="tabbable-line tabbable-full-width">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#tab_1_1" data-toggle="tab"> Overview </a>
            </li>
            <li>
                <a href="#tab_1_3" data-toggle="tab"> Account </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1_1">
               <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 margin-bottom-10">
                            <div class="dashboard-stat blue">
                                <div class="visual">
                                    <i class="fa fa-briefcase fa-icon-medium"></i>
                                </div>
                                <div class="details">
                                    <div class="number">{{$totalcars}} </div>
                                    <div class="desc"> Total Cars </div>
                                </div>
                                <a class="more" href="javascript:;"> View All   
                                    <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat red">
                                <div class="visual">
                                    <i class="fa fa-shopping-cart"></i>
                                </div>
                                <div class="details">
                                    <div class="number"> 10,000 </div>
                                    <div class="desc"> Active </div>
                                </div>
                                <a class="more" href="javascript:;"> View more
                                    <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat green">
                                <div class="visual">
                                    <i class="fa fa-group fa-icon-medium"></i>
                                </div>
                                <div class="details">
                                    <div class="number"> 43,000 </div>
                                    <div class="desc"> Complete </div>
                                </div>
                                <a class="more" href="javascript:;"> View more
                                    <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>
                    </div>
               <div class="row" style="margin-top:50px;">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-3">
                                    <ul class="list-unstyled profile-nav">
                                        <li>
                                            <img src="{{ asset(path_profile().'avatar.jpg') }}" class="img-responsive pic-bordered" alt="" />
                                        </li>
                                        <li>
                                            <a href="javascript:;"> Projects </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;"> Messages
                                                <span> 3 </span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-9">
                                    <div class="row">
                                        <div class="col-md-8 profile-info">
                                            <h1 class="font-green sbold uppercase">{{$company->name}}</h1>
                                            <p> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt laoreet dolore magna aliquam tincidunt erat volutpat laoreet dolore magna aliquam tincidunt erat volutpat.
                                                </p>
                                            <p>
                                                <a href="javascript:;"> {{$company->website}} </a>
                                            </p>
                                            <ul class="list-inline">
                                                <li>
                                                    <i class="fa fa-map-marker"></i> Spain </li>
                                                <li>
                                                    <i class="fa fa-calendar"></i> 18 Jan 1982 </li>
                                                <li>
                                                    <i class="fa fa-briefcase"></i> Design </li>
                                                <li>
                                                    <i class="fa fa-star"></i> Top Seller </li>
                                                <li>
                                                    <i class="fa fa-heart"></i> BASE Jumping </li>
                                            </ul>
                                        </div>
                                        <!--end col-md-8-->
                                        <div class="col-md-4">
                                            <div class="portlet sale-summary">
                                                <div class="portlet-title">
                                                    <div class="caption font-red sbold"> Sales Summary </div>
                                                    <div class="tools">
                                                        <a class="reload" href="javascript:;"> </a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body">
                                                    <ul class="list-unstyled">
                                                        <li>
                                                            <span class="sale-info"> TODAY SOLD
                                                                <i class="fa fa-img-up"></i>
                                                            </span>
                                                            <span class="sale-num"> 23 </span>
                                                        </li>
                                                        <li>
                                                            <span class="sale-info"> WEEKLY SALES
                                                                <i class="fa fa-img-down"></i>
                                                            </span>
                                                            <span class="sale-num"> 87 </span>
                                                        </li>
                                                        <li>
                                                            <span class="sale-info"> TOTAL SOLD </span>
                                                            <span class="sale-num"> 2377 </span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end col-md-4-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="caption font-blue-sharp">
                            <i class="icon-plus font-blue-sunglo"></i>
                            <span class="caption-subject bold uppercase"> Car List</span>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                            <thead>
                                <tr>
                                    <th>
                                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                            <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                                            <span></span>
                                        </label>
                                    </th>
                                    <th>Car Barcode</th>
                                    <th>Comapny Id</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(isset($cars) && (count($cars) > 0))
                                @foreach($cars as $car)
                                    <tr class="odd gradeX">
                                        <td>
                                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                <input type="checkbox" class="checkboxes" value="{{ $car->id }} " />
                                                <span></span>
                                            </label>
                                        </td>
                                        <td>
                                            <a href="{{ route('company',['id'=>$car->id, 'lang'=>$lang]) }}">{{ $car->cbarcode }}</a>
                                        </td>
                                        <td>
                                            {{$car->rccompany_id}}
                                        </td>
                                    </tr>
                                @endforeach  
                             @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!--tab_1_2-->
            <div class="tab-pane" id="tab_1_3">
                <div class="row profile-account">
                    <div class="col-md-12">
                        <div class="tab-content">
                            <div id="tab_1-1" class="tab-pane active">
                                {{ Form::open(['route' =>['companyupdateprocess','id'=>$id,'lang'=>$lang]]) }}
                                    <div class="form-body">
                                      <div class="form-group">
                                          <div class="row">
                                            <div class="col-md-12">
                                              <label class="control-label">Company Name</label>
                                              <input type="text" name="name" placeholder="{{$company->name}}" class="form-control" value="{{$company->name}}" /> 
                                            </div>
                                                </div>  
                                      </div>  
                                      <div class="form-group">
                                          <div class="row">
                                            <div class="col-md-6">
                                               <label>Email Address</label>
                                                  <div class="input-group">
                                                      <span class="input-group-addon">
                                                        <i class="fa fa-envelope"></i>
                                                      </span>
                                                      <input type="text" name="email" class="form-control" placeholder="{{$company->email}}" value="{{$company->email}}"> 
                                                  </div>
                                            </div>
                                            <div class="col-md-6">
                                              <label class="control-label">Website Url</label>
                                              <input type="text" name="website" placeholder="{{$company->website}}" class="form-control" value="{{$company->website}}" /> 
                                            </div>
                                          </div>
                                      </div>
                                      <div class="form-group">
                                          <div class="row">
                                            <div class="col-md-6">
                                              <label class="control-label">Street One</label>
                                              <input type="text" name="streetone" placeholder="{{$company->streetone}}" class="form-control" value="{{$company->streetone}}" /> 
                                            </div>
                                            <div class="col-md-6">
                                              <label class="control-label">Street Two</label>
                                              <input type="text" name="streettwo" placeholder="{{$company->streettwo}}" class="form-control" value="{{$company->streetone}}" /> 
                                            </div>
                                          </div>  
                                      </div>     
                                      <div class="form-group">
                                          <div class="row">
                                            <div class="col-md-6">
                                               <input type="hidden" id="hiddenphone" name="phonenumber" />
                                               <label class="control-label">Mobile Number</label>
                                               <input type="tel" id="phonenumber" value="{{ $company->phonenumber }}" placeholder="01243566" class="form-control" name="phonenumber"/>
                                            </div>
                                            <div class="col-md-6">
                                              <label class="control-label">Post Code</label>
                                              <input type="text" name="postcode" placeholder="{{$company->postcode}}" class="form-control" value="{{$company->postcode}}" /> 
                                            </div>
                                          </div>  
                                      </div>
                                      <div class="form-group">
                                          <div class="row">
                                                <div class="col-md-6">
                                                    <label>Country</label>
                                                   <select class="form-control" name="country">
                                                        @foreach(country() as $key => $val)
                                                        <option value="{{$key}}"<?php if(old('country')!=null && old('country')==$key){echo ' selected';}
                                                             elseif($company->country==$key){echo ' selected';}
                                                        ?>>{{$val}}</option>
                                                      @endforeach
                                                     </select>
                                                </div>
                                                <div class="col-md-6">
                                                   <label class="control-label">State</label>
                                                 <input type="text" name="state" placeholder="State" class="form-control" value="{{$company->state}}" />
                                                </div>
                                           </div> 
                                      </div>                           
                                      <div class="form-actions">
                                          <button type="submit" class="btn blue">Save Change</button>
                                      </div>
                                    </div>
                               {{ Form::close() }} 
                            </div>
                            <div id="tab_2-2" class="tab-pane">
                                <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum
                                    eiusmod. </p>
                            </div>
                        </div>
                    </div>
                    <!--end col-md-9-->
                </div>
            </div>
            <!--end tab-pane-->
            </div>
        </div>
    </div>

</div>     
</div>
@endsection                            
@section('afterscripts')
{{Html::script('assets/js/JsBarcode.all.min.js')}} 
{{Html::script('assets/js/datepicker.js')}} 
{{Html::script('assets/vendors/build/js/intlTelInput.js')}}
<script type="text/javascript">
  //Init jquery Date Picker
  $('.datepicker').datepicker({
     format: 'yyyy-mm-dd',
     autoclose: true,
     orientation: 'bottom'
 });

         // for verify phone country code
         $("#phonenumber").intlTelInput({
        // allowDropdown: false,
        autoHideDialCode: false,
        // autoPlaceholder: "off",
        // dropdownContainer: "body",
        // excludeCountries: ["us"],
        // formatOnDisplay: true,
        // geoIpLookup: function(callback) {
        //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
        //     var countryCode = (resp && resp.country) ? resp.country : "";
        //     callback(countryCode);
        //   });
        // },
        // initialCountry: "auto",
        // nationalMode: false,
        // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
        // placeholderNumberType: "MOBILE",
        // preferredCountries: ['cn', 'jp'],
        separateDialCode: true,
        utilsScript: "{{ asset('assets/vendors/build/js/utils.js') }}"
    });

         $('#phonenumber').on('change', function() {
            $('#hiddenphone').val($("#phonenumber").intlTelInput("getNumber"));
        });

         JsBarcode("#barcode1", $('#barcode').val());

     </script>
     @endsection
     
     @section('afterstyle')
     {{Html::style('assets/css/datepicker.css')}}
     {{Html::style('assets/vendors/build/css/intlTelInput.css')}}
     @endsection