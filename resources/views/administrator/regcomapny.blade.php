@extends('layoutmaster')
@section('mainbody')

<div class="row">
   <div class="col-md-10 col-md-offset-1">
     <div class="portlet light">
         <div class="portlet-title">
            <div class="caption font-blue-sharp">
                <i class="icon-plus font-blue-sunglo"></i>
                <span class="caption-subject bold uppercase">Register Comapny</span>
            </div>
          </div>
          <div class="portlet-body form">
           {{ Form::open(['route' =>['regcomapnyprocess','lang'=>$lang]]) }}
                <div class="form-body">
                  <div class="form-group">
                      <div class="row">
                        <div class="col-md-12">
                          <label class="control-label">Comapny Name</label>
                          <input type="text" name="name" placeholder="Comapny Name" class="form-control" /> 
                        </div>
                  			</div>	
                  </div>  
                  <div class="form-group">
                      <div class="row">
                        <div class="col-md-6">
                           <label>Email Address</label>
                              <div class="input-group">
                                  <span class="input-group-addon">
                                    <i class="fa fa-envelope"></i>
                                  </span>
                                  <input type="text" name="email" class="form-control" placeholder="Email Address"> 
                              </div>
                        </div>
                        <div class="col-md-6">
                          <label class="control-label">Website Url</label>
                          <input type="text" name="website" placeholder="Website" class="form-control" /> 
                        </div>
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="row">
                        <div class="col-md-6">
                          <label class="control-label">Street One</label>
                          <input type="text" name="streetone" placeholder="Street One" class="form-control" /> 
                        </div>
                        <div class="col-md-6">
                          <label class="control-label">Street Two</label>
                          <input type="text" name="streettwo" placeholder="Street Two" class="form-control" /> 
                        </div>
                      </div>  
                  </div>     
                  <div class="form-group">
                      <div class="row">
                        <div class="col-md-6">
                           <input type="hidden" id="hiddenphone" name="phonenumber" />
                           <label class="control-label">Mobile Number</label>
                           <input type="tel" id="phonenumber" placeholder="01243566" class="form-control" />
                        </div>
                        <div class="col-md-6">
                          <label class="control-label">Post Code</label>
                          <input type="text" name="postcode" placeholder="Post Code" class="form-control" /> 
                        </div>
                      </div>  
                  </div>
                  <div class="form-group">
                      <div class="row">
                            <div class="col-md-6">
                                <label>Country</label>
                                <select class="form-control" name="country">
                                  @foreach(country() as $key => $val)
                                    <option value="{{$key}}"<?php if(old('country')!=null && old('country')==$key){echo ' selected';}?>>{{$val}}</option>
                                  @endforeach
                                </select>
                            </div>
                            <div class="col-md-6">
                               <label class="control-label">State</label>
                             <input type="text" name="state" placeholder="State" class="form-control" />
                            </div>
                       </div> 
                  </div>                           
                  <div class="form-actions">
                      <button type="submit" class="btn blue">Submit</button>
                      <button type="button" class="btn default">Cancel</button>
                  </div>
                </div>
             {{ Form::close() }} 
      </div>
   </div>
</div>

@endsection
@section('afterscripts')
{{Html::script('assets/js/datepicker.js')}} 
 {{Html::script('assets/vendors/build/js/intlTelInput.js')}}
 <script type="text/javascript">
  //Init jquery Date Picker
        $('.datepicker').datepicker({
             format: 'yyyy-mm-dd',
             autoclose: true,
             orientation: 'bottom'
         });

         // for verify phone country code
      $("#phonenumber").intlTelInput({
        // allowDropdown: false,
        autoHideDialCode: false,
        // autoPlaceholder: "off",
        // dropdownContainer: "body",
        // excludeCountries: ["us"],
        // formatOnDisplay: true,
        // geoIpLookup: function(callback) {
        //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
        //     var countryCode = (resp && resp.country) ? resp.country : "";
        //     callback(countryCode);
        //   });
        // },
        // initialCountry: "auto",
        // nationalMode: false,
        // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
        // placeholderNumberType: "MOBILE",
        // preferredCountries: ['cn', 'jp'],
        separateDialCode: true,
        utilsScript: "{{ asset('assets/vendors/build/js/utils.js') }}"
      });
      $('#phonenumber').on('change', function() {
        $('#hiddenphone').val($("#phonenumber").intlTelInput("getNumber"));
      });
 </script>
@endsection
 
@section('afterstyle')
 {{Html::style('assets/css/datepicker.css')}}
 {{Html::style('assets/vendors/build/css/intlTelInput.css')}}
 @endsection