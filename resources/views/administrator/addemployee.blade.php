@extends('layoutmaster')

@section('mainbody')

<div class="row">
   <div class="col-md-10 col-md-offset-1">
     <div class="portlet light">
         <div class="portlet-title">
            <div class="caption font-blue-sharp">
                <i class="icon-plus font-blue-sunglo"></i>
                <span class="caption-subject bold uppercase">Register Employee</span>
            </div>
          </div>
          <div class="portlet-body form">
           {{ Form::open(['route' => ['addemployeeprocess', 'lang'=>$lang]]) }}
	        <form role="form">
	            <div class="form-body">
	                <div class="form-group">
	                    <div class="row">
         							<div class="col-md-6">
         							    <label class="control-label">First Name</label>
      	                  <input type="text" name="fname" placeholder="John" class="form-control" value="<?php if(old('fname')!=null ){echo old('fname');} ?>"/> 
         							</div>
         							<div class="col-md-6">
      	                 <label class="control-label">Last Name</label>
                          <input type="text" name="lname" placeholder="Doe" class="form-control" value="<?php if(old('lname')!=null ){echo old('lname');} ?>" /> 
                      </div>
                    </div>  
                  </div>
                  <div class="form-group">
                      <div class="row">
                        <div class="col-md-6">
                          <label>Email Address</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-envelope"></i>
                                </span>
                                <input type="text" name="email" class="form-control" placeholder="Email Address" value="<?php if(old('email')!=null ){echo old('email');} ?>"> 
                            </div>
                        </div>
                <div class="col-md-6">
                  <input type="hidden" id="hiddenphone" name="phonenumber" />
                   <label class="control-label">Mobile Number</label>
                   <input type="tel" id="phonenumber" placeholder="01243566" class="form-control" value="<?php if(old('phonenumber')!=null ){echo old('phonenumber');} ?>"/>
   							</div>
   						</div>	
	                </div>
	                <div class="form-group">
	                    <div class="row">
             							<div class="col-md-6">
             							    <label>Country</label>
            	                  <select class="form-control" name="country">
            	                    @foreach(country() as $key => $val)
                                    <option value="{{$key}}"<?php if(old('country')!=null && old('country')==$key){echo ' selected';}?>>{{$val}}</option>
                                  @endforeach
            	                  </select>
             							</div>
             							<div class="col-md-6">
             							   <label class="control-label">Address</label>
          	                 <input type="text" name="address" class="form-control" value="<?php if(old('address')!=null ){echo old('address');} ?>"/>
   							          </div>
   						       </div>	
	                </div>  
	                <div class="form-group">
	                    <div class="row">
             							<div class="col-md-6">
             							   <label class="control-label">Date of Birth</label> 
                               <input type="text" name="dob" placeholder="0000-00-00" class="form-control datepicker" value="<?php if(old('dob')!=null ){echo old('dob');} ?>"> 
             							</div>
             							<div class="col-md-6">
             							    <label>Gender</label>
          	                      <select class="form-control" name="gender">
                                    @foreach(gender() as $input => $value)
                                      <option value="{{$input}}" <?php if(old('gender')!=null && old('gender')==$input){echo ' selected';}?>>{{$value}}
                                      </option>
                                    @endforeach
          	                      </select>
             							</div>
   						        </div>	
	                </div>
	                <div class="form-group">
	                    <div class="row">
           							<div class="col-md-6">
           							    <label>Department</label>
                            @if(isset($departments))
        	                  <select class="form-control" name="department">
                              @foreach($departments as $department)
        	                    <option value="{{$department->id}}">{{$department->name}}</option>
                              @endforeach
        	                  </select>
                            @endif
           							</div>
           							<div class="col-md-6">
           							   <label class="control-label">Joint Date</label> 
                             <input type="text" name="jdate" placeholder="0000-00-00" class="form-control datepicker" value="<?php if(old('jdate')!=null ){echo old('jdate');} ?>">
           							</div>
   						       </div>
   					      </div>	
                  <div class="form-group">
                      <div class="row">
                        <div class="col-md-12">
                            <div class="md-checkbox">
                              <input type="checkbox" name="user" value="0" id="checkbox1_1" class="md-check">
                              <label for="checkbox1_1">
                                  <span></span>
                                  <span class="check"></span>
                                  <span class="box"></span>Make User</label>
                            </div>
                        </div>    
                      </div>
                  </div>  
                   <div class="form-group" id="userdata">
                      <div class="row">
                        <div class="col-md-6">
                            <label class="control-label">User Role</label>
                            <select class="form-control" name="role">
                                    @foreach(role() as $input => $val)
                                      <option value="{{$input}}" <?php if(old('role')!=null && old('role')==$input){echo ' selected';}?>>{{$val}}
                                      </option>
                                    @endforeach
                            </select>
                        </div>
                        <div class="col-md-6">
                           <label class="control-label">Password</label> 
                             <input type="password" name="password"  class="form-control" value="">
                        </div>
                     </div>
                  </div>                                   
	                <div class="form-actions">
    	                <button type="submit" class="btn blue">Submit</button>
    	                <button type="button" class="btn default">Cancel</button>
	               </div>
             {{ Form::close() }}        
	        </form>
        </div>
      </div>
   </div>
</div>

@endsection
@section('afterscripts')
{{Html::script('assets/js/datepicker.js')}} 
 {{Html::script('assets/vendors/build/js/intlTelInput.js')}}
 <script type="text/javascript">
  //Init jquery Date Picker
        $('.datepicker').datepicker({
             format: 'yyyy-mm-dd',
             autoclose: true,
             orientation: 'bottom'
         });

         // for verify phone country code
      $("#phonenumber").intlTelInput({
        // allowDropdown: false,
        autoHideDialCode: false,
        // autoPlaceholder: "off",
        // dropdownContainer: "body",
        // excludeCountries: ["us"],
        // formatOnDisplay: true,
        // geoIpLookup: function(callback) {
        //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
        //     var countryCode = (resp && resp.country) ? resp.country : "";
        //     callback(countryCode);
        //   });
        // },
        // initialCountry: "auto",
        // nationalMode: false,
        // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
        // placeholderNumberType: "MOBILE",
        // preferredCountries: ['cn', 'jp'],
        separateDialCode: true,
        utilsScript: "{{ asset('assets/vendors/build/js/utils.js') }}"
      });

      $('#phonenumber').on('change', function() {
        $('#hiddenphone').val($("#phonenumber").intlTelInput("getNumber"));
      });

      $("#userdata").hide();
      $('#checkbox1_1').on('change', function() {
      if ( $('#checkbox1_1').val() == '0')
      {
        $('#checkbox1_1').val('1')
        $("#userdata").show();
       
      }
      else  
      {
        $('#checkbox1_1').val('0')
        $("#userdata").hide();
      }
    });

 </script>
@endsection
 
@section('afterstyle')
 {{Html::style('assets/css/datepicker.css')}}
 {{Html::style('assets/vendors/build/css/intlTelInput.css')}}
@endsection