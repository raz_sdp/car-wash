@extends('layoutmaster')
@section('mainbody')

<div class="row">
   <div class="col-md-10 col-md-offset-1">
     <div class="portlet light">
         <div class="portlet-title">
            <div class="caption font-blue-sharp">
                <i class="icon-plus font-blue-sunglo"></i>
                <span class="caption-subject bold uppercase">Register Car</span>
            </div>
          </div>
          <div class="portlet-body form">
           {{ Form::open(['route' =>['regcarprocess','lang'=>$lang]]) }}
                <div class="form-body">
                  <div class="form-group">
                      <div class="row">
                        <div class="col-md-12">
                          <label class="control-label">Bar code</label>
                          <input type="text" name="cbarcode" placeholder="Car barcode" class="form-control" /> 
                        </div>
                  		</div>	
                  </div>  
                  <div class="form-group">
                      <div class="row">
                        <div class="col-md-12">
                            <label>Company</label>
                            @if(isset($companies))
                            <select class="form-control" name="company">
                              @foreach($companies as $company)
                              <option value="{{$company->id}}">{{$company->name}}</option>
                              @endforeach
                            </select>
                            @endif
                        </div>
                     </div>
                  </div>                         
                  <div class="form-actions">
                      <button type="submit" class="btn blue">Submit</button>
                      <button type="button" class="btn default">Cancel</button>
                  </div>
                </div>
             {{ Form::close() }} 
      </div>
   </div>
</div>

@endsection
