
@extends('layoutmaster')
@section('afterstyle')
{{Html::style('assets/vendors/jquery-file-upload/css/jquery.fileupload.css')}}
{{Html::style('assets/vendors/bootstrap-fileinput/bootstrap-fileinput.css')}}
@endsection
@section('mainbody')
<div class="row">
     <div class="col-md-12">        
       <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title"> User Profile 
                            <small>view</small>
                        </h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                        <div class="profile">
                            <div class="tabbable-line tabbable-full-width">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#tab_1_1" data-toggle="tab"> Overview </a>
                                    </li>
                                    <li>
                                        <a href="#tab_1_3" data-toggle="tab"> Account </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_1_1">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <ul class="list-unstyled profile-nav">
                                                    <li>
                                                        <img src="{{ asset(path_profile().'profile.jpg') }}" class="img-responsive pic-bordered" alt="" />
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;"> Report </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;"> Pending Task
                                                            <span> 3 </span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;"> Settings </a>
                                                    </li>
                                                    <li>
                                                        <input type="hidden" id="barcode" name="barcode" value="{{ $employee->ubarcode }}"/>
                                                       <img id="barcode1" style="width:210px; height=210px"  />
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="row">
                                                    <div class="col-md-8 profile-info">
                                                        <h1 class="font-green sbold uppercase">{{ $employee->fname }}  {{ $employee->lname }}</h1>
                                                        <p> {{$employee->about}}
                                                        </p>
                                                        
                                                        <ul class="list-inline">
                                                            <li>
                                                                <i class="fa fa-map-marker"></i> {{ country($employee->country) }} </li>
                                                            <li>
                                                                <i class="fa fa-calendar"></i> {{ $employee->joindate }} </li>
                                                            <li>
                                                                <i class="fa fa-briefcase"></i> {{ $employee->rcdepartment_id }}  </li>
                                                            @if(isset($employee->rcuser) && (count($employee->rcuser) > 0) )    
                                                            <li>
                                                                <i class="fa fa-star"></i> {{ role($employee->rcuser->acstatus) }} </li>
                                                            <li>
                                                                <i class="fa fa-heart"></i> {{ activelevel($employee->rcuser->acstatus) }} </li>
                                                            @else
                                                            <li>
                                                                <i class="fa fa-star"></i> Employee 
                                                            </li> 
                                                            @endif   
                                                        </ul>
                                                    </div>
                                                    <!--end col-md-8-->
                                                    <div class="col-md-4">
                                                        <div class="portlet sale-summary">
                                                            <div class="portlet-title">
                                                                <div class="caption font-red sbold"> Task Summary </div>
                                                                <div class="tools">
                                                                    <a class="reload" href="javascript:;"> </a>
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body">
                                                                <ul class="list-unstyled">
                                                                    <li>
                                                                        <span class="sale-info"> TODAY Complete Task
                                                                            <i class="fa fa-img-up"></i>
                                                                        </span>
                                                                        <span class="sale-num"> 23 </span>
                                                                    </li>
                                                                    <li>
                                                                        <span class="sale-info"> WEEKLY Task
                                                                            <i class="fa fa-img-down"></i>
                                                                        </span>
                                                                        <span class="sale-num"> 87 </span>
                                                                    </li>
                                                                    <li>
                                                                        <span class="sale-info"> TOTAL Task </span>
                                                                        <span class="sale-num"> 2377 </span>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--end col-md-4-->
                                                </div>
                                                <!--end row-->
                                                <div class="tabbable-line tabbable-custom-profile">
                                                    <ul class="nav nav-tabs">
                                                        <li class="active">
                                                            <a href="#tab_1_11" data-toggle="tab"> Latest Task </a>
                                                        </li>
                                                        <li>
                                                            <a href="#tab_1_22" data-toggle="tab"> Feeds </a>
                                                        </li>
                                                    </ul>
                                                    <div class="tab-content">
                                                        <div class="tab-pane active" id="tab_1_11">
                                                            <div class="portlet-body">
                                                                <table class="table table-striped table-bordered table-advance table-hover">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>
                                                                                <i class="fa fa-briefcase"></i> Company </th>
                                                                            <th class="hidden-xs">
                                                                                <i class="fa fa-question"></i> Descrition </th>
                                                                            <th>
                                                                                <i class="fa fa-bookmark"></i> Amount </th>
                                                                            <th> </th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td>
                                                                                <a href="javascript:;"> Pixel Ltd </a>
                                                                            </td>
                                                                            <td class="hidden-xs"> Refuel Car </td>
                                                                            <td> 52560
                                                                                <span class="label label-success label-sm"> Complete </span>
                                                                            </td>
                                                                            <td>
                                                                                <a class="btn btn-sm grey-salsa btn-outline" href="javascript:;"> View </a>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <a href="javascript:;"> Smart House </a>
                                                                            </td>
                                                                            <td class="hidden-xs"> Washing House </td>
                                                                            <td> 5760
                                                                                <span class="label label-warning label-sm"> Pending </span>
                                                                            </td>
                                                                            <td>
                                                                                <a class="btn btn-sm grey-salsa btn-outline" href="javascript:;"> View </a>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <a href="javascript:;"> FoodMaster Ltd </a>
                                                                            </td>
                                                                            <td class="hidden-xs"> Company Anual Dinner Catering </td>
                                                                            <td> 12400
                                                                                <span class="label label-success label-sm"> Complete </span>
                                                                            </td>
                                                                            <td>
                                                                                <a class="btn btn-sm grey-salsa btn-outline" href="javascript:;"> View </a>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <!--tab-pane-->
                                                        <div class="tab-pane" id="tab_1_22">
                                                            <div class="tab-pane active" id="tab_1_1_1">
                                                                <div class="scroller" data-height="290px" data-always-visible="1" data-rail-visible1="1">
                                                                    <ul class="feeds">
                                                                        <li>
                                                                            <div class="col1">
                                                                                <div class="cont">
                                                                                    <div class="cont-col1">
                                                                                        <div class="label label-success">
                                                                                            <i class="fa fa-bell-o"></i>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="cont-col2">
                                                                                        <div class="desc"> You have 4 pending tasks.
                                                                                            <span class="label label-danger label-sm"> Take action
                                                                                                <i class="fa fa-share"></i>
                                                                                            </span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col2">
                                                                                <div class="date"> Just now </div>
                                                                            </div>
                                                                        </li>
                                                                        <li>
                                                                            <a href="javascript:;">
                                                                                <div class="col1">
                                                                                    <div class="cont">
                                                                                        <div class="cont-col1">
                                                                                            <div class="label label-success">
                                                                                                <i class="fa fa-bell-o"></i>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="cont-col2">
                                                                                            <div class="desc"> New version v1.4 just lunched! </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col2">
                                                                                    <div class="date"> 20 mins </div>
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <div class="col1">
                                                                                <div class="cont">
                                                                                    <div class="cont-col1">
                                                                                        <div class="label label-danger">
                                                                                            <i class="fa fa-bolt"></i>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="cont-col2">
                                                                                        <div class="desc"> Database server #12 overloaded. Please fix the issue. </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col2">
                                                                                <div class="date"> 24 mins </div>
                                                                            </div>
                                                                        </li>
                                                                        <li>
                                                                            <div class="col1">
                                                                                <div class="cont">
                                                                                    <div class="cont-col1">
                                                                                        <div class="label label-info">
                                                                                            <i class="fa fa-bullhorn"></i>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="cont-col2">
                                                                                        <div class="desc"> New order received. Please take care of it. </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col2">
                                                                                <div class="date"> 30 mins </div>
                                                                            </div>
                                                                        </li>
                                                                        <li>
                                                                            <div class="col1">
                                                                                <div class="cont">
                                                                                    <div class="cont-col1">
                                                                                        <div class="label label-success">
                                                                                            <i class="fa fa-bullhorn"></i>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="cont-col2">
                                                                                        <div class="desc"> New order received. Please take care of it. </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col2">
                                                                                <div class="date"> 40 mins </div>
                                                                            </div>
                                                                        </li>
                                                                        <li>
                                                                            <div class="col1">
                                                                                <div class="cont">
                                                                                    <div class="cont-col1">
                                                                                        <div class="label label-warning">
                                                                                            <i class="fa fa-plus"></i>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="cont-col2">
                                                                                        <div class="desc"> New user registered. </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col2">
                                                                                <div class="date"> 1.5 hours </div>
                                                                            </div>
                                                                        </li>
                                                                        <li>
                                                                            <div class="col1">
                                                                                <div class="cont">
                                                                                    <div class="cont-col1">
                                                                                        <div class="label label-success">
                                                                                            <i class="fa fa-bell-o"></i>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="cont-col2">
                                                                                        <div class="desc"> Web server hardware needs to be upgraded.
                                                                                            <span class="label label-inverse label-sm"> Overdue </span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col2">
                                                                                <div class="date"> 2 hours </div>
                                                                            </div>
                                                                        </li>
                                                                        <li>
                                                                            <div class="col1">
                                                                                <div class="cont">
                                                                                    <div class="cont-col1">
                                                                                        <div class="label label-default">
                                                                                            <i class="fa fa-bullhorn"></i>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="cont-col2">
                                                                                        <div class="desc"> New order received. Please take care of it. </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col2">
                                                                                <div class="date"> 3 hours </div>
                                                                            </div>
                                                                        </li>
                                                                        <li>
                                                                            <div class="col1">
                                                                                <div class="cont">
                                                                                    <div class="cont-col1">
                                                                                        <div class="label label-warning">
                                                                                            <i class="fa fa-bullhorn"></i>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="cont-col2">
                                                                                        <div class="desc"> New order received. Please take care of it. </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col2">
                                                                                <div class="date"> 5 hours </div>
                                                                            </div>
                                                                        </li>
                                                                        <li>
                                                                            <div class="col1">
                                                                                <div class="cont">
                                                                                    <div class="cont-col1">
                                                                                        <div class="label label-info">
                                                                                            <i class="fa fa-bullhorn"></i>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="cont-col2">
                                                                                        <div class="desc"> New order received. Please take care of it. </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col2">
                                                                                <div class="date"> 18 hours </div>
                                                                            </div>
                                                                        </li>
                                                                     </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--tab-pane-->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--tab_1_2-->
                                    <div class="tab-pane" id="tab_1_3">
                                        <div class="row profile-account">
                                            <div class="col-md-3">
                                                <ul class="ver-inline-menu tabbable margin-bottom-10">
                                                    <li class="active">
                                                        <a data-toggle="tab" href="#tab_1-1">
                                                            <i class="fa fa-cog"></i> Personal info </a>
                                                        <span class="after"> </span>
                                                    </li>
                                                    <li>
                                                        <a data-toggle="tab" href="#tab_2-2">
                                                            <i class="fa fa-picture-o"></i> Change Avatar </a>
                                                    </li>
                                                    <li>
                                                        <a data-toggle="tab" href="#tab_3-3">
                                                            <i class="fa fa-lock"></i> Change Password </a>
                                                    </li>
                                                    <li>
                                                        <a data-toggle="tab" href="#tab_4-4">
                                                            <i class="fa fa-eye"></i> Privacity Settings </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="tab-content">
                                                    <div id="tab_1-1" class="tab-pane active">
                                                       {{ Form::open(['route' => ['editemployeeprocess','lang'=>$lang]]) }}
                                                             <input type="hidden" name="id" value="{{ $employee->id }}" /> 
                                                            <div class="form-group">
                                                               <div class="row">
                                                                    <div class="col-md-6">
                                                                       <label class="control-label">First Name</label>
                                                                       <input type="text" placeholder="John" class="form-control" name="fname"
                                                                       value="{{ $employee->fname }}" />
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                       <label class="control-label">Last Name</label>
                                                                       <input type="text" placeholder="Doe" class="form-control" name="lname" value="{{ $employee->lname }}"/> 
                                                                    </div>
                                                               </div>
                                                            </div> 
                                                             <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                      <label>Email Address</label>
                                                                        <div class="input-group">
                                                                            <span class="input-group-addon">
                                                                                <i class="fa fa-envelope"></i>
                                                                            </span>
                                                                            <input type="text" name="email" class="form-control" placeholder="Email Address" value="{{ $employee->email }}"> 
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                      <input type="hidden" id="hiddenphone" name="phonenumber" value="{{ $employee-> phonenumber }}" />
                                                                       <label class="control-label">Mobile Number</label>
                                                                       <input type="tel" id="phonenumber" value="{{ $employee-> phonenumber }}" placeholder="01243566" class="form-control" />
                                                                    </div>
                                                                </div>  
                                                            </div>
                                                               <div class="form-group">
                                                                    <div class="row">
                                                                                    <div class="col-md-6">
                                                                                        <label>Country</label>
                                                                              <select class="form-control" name="country">
                                                                                @foreach(country() as $key => $val)
                                                                                <option value="{{$key}}"<?php if(old('country')!=null && old('country')==$key){echo ' selected';}
                                                                                     elseif($employee->country==$key){echo ' selected';}
                                                                                ?>>{{$val}}</option>
                                                                              @endforeach
                                                                              </select>
                                                                                    </div>
                                                                                 <div class="col-md-6">
                                                                                       <label class="control-label">Date of Birth</label> 
                                                                           <input type="text" name="dob" placeholder="0000-00-00" class="form-control datepicker" value="{{ $employee-> dob }}"> 
                                                                                    </div>    
                                                                           </div>   
                                                                </div>  
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                                   
                                                                                    <div class="col-md-6">
                                                                                        <label>Gender</label>
                                                                              <select class="form-control" name="gender">
                                                                                @foreach(gender() as $input => $value)
                                                                                  <option value="{{$input}}" <?php if(old('gender')!=null && old('gender')==$input){echo ' selected';} elseif($employee->gender==$key){echo ' selected';}
                                                                                  ?>>{{$value}}
                                                                                  </option>
                                                                                @endforeach
                                                                              </select>
                                                                                    </div>
                                                                                      <div class="col-md-6">
                                                                                   <label class="control-label">Joint Date</label> 
                                                                         <input type="text" name="jdate" placeholder="0000-00-00" class="form-control datepicker" value="{{$employee-> joindate}}">
                                                                                </div>
                                                                            </div>  
                                                                </div>
                                                                    
                                                               
                                                               <div class="form-group" >
                                                                    <label class="control-label">Address</label>
                                                                    <input type="text" name="address" value="{{ $employee->address }}" class="form-control" /> 
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="control-label">About</label>
                                                                    <textarea class="form-control" rows="3" name="about">{{$employee->about}}</textarea>
                                                                </div>
                                                                
                                                                <div class="margiv-top-10">
                                                                    <button type="submit "class="btn green"> Save Changes </button>
                                                                    <button class="btn default"> Cancel </button>
                                                                </div>
                                                            {{ Form::close() }}


                                                    </div>
                                                    <div id="tab_2-2" class="tab-pane">
                                                       <form action="#" role="form">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-sm-6">
                                                                        <label>Profile Image. Recommended: 300px by 300px</label><br>
                                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                            <div class="fileinput-new thumbnail" style="width: 150px; height: 150px;">
                                                                                <img src="{{asset('assets/img/profiles/profile.jpg')}}" alt="" /> </div>
                                                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 150px; max-height:150px;"> </div>
                                                                            <div>
                                                                                <span class="btn default btn-file">
                                                                                    <span class="fileinput-new"> Select image </span>
                                                                                    <span class="fileinput-exists"> Change </span>
                                                                                    <input type="file" name="thumb"> </span>
                                                                                <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                               </div>
                                                            </div>
                                                            <div class="margin-top-10">
                                                                <button  type="submit" class="btn green"> Submit </button>
                                                                <button  class="btn default"> Cancel </button>
                                                            </div>
                                                     </form>
                                                    </div>
                                                    <div id="tab_3-3" class="tab-pane">
                                                        <form action="#">
                                                            <div class="form-group">
                                                                <label class="control-label">Current Password</label>
                                                                <input type="password" class="form-control" /> </div>
                                                            <div class="form-group">
                                                                <label class="control-label">New Password</label>
                                                                <input type="password" class="form-control" /> </div>
                                                            <div class="form-group">
                                                                <label class="control-label">Re-type New Password</label>
                                                                <input type="password" class="form-control" /> </div>
                                                            <div class="margin-top-10">
                                                                <a href="javascript:;" class="btn green"> Change Password </a>
                                                                <a href="javascript:;" class="btn default"> Cancel </a>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <div id="tab_4-4" class="tab-pane">
                                                        <form action="#">
                                                            <table class="table table-bordered table-striped">
                                                                <tr>
                                                                    <td> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus.. </td>
                                                                    <td>
                                                                        <div class="mt-radio-inline">
                                                                            <label class="mt-radio">
                                                                                <input type="radio" name="optionsRadios1" value="option1" /> Yes
                                                                                <span></span>
                                                                            </label>
                                                                            <label class="mt-radio">
                                                                                <input type="radio" name="optionsRadios1" value="option2" checked/> No
                                                                                <span></span>
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td> Enim eiusmod high life accusamus terry richardson ad squid wolf moon </td>
                                                                    <td>
                                                                        <div class="mt-radio-inline">
                                                                            <label class="mt-radio">
                                                                                <input type="radio" name="optionsRadios21" value="option1" /> Yes
                                                                                <span></span>
                                                                            </label>
                                                                            <label class="mt-radio">
                                                                                <input type="radio" name="optionsRadios21" value="option2" checked/> No
                                                                                <span></span>
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td> Enim eiusmod high life accusamus terry richardson ad squid wolf moon </td>
                                                                    <td>
                                                                        <div class="mt-radio-inline">
                                                                            <label class="mt-radio">
                                                                                <input type="radio" name="optionsRadios31" value="option1" /> Yes
                                                                                <span></span>
                                                                            </label>
                                                                            <label class="mt-radio">
                                                                                <input type="radio" name="optionsRadios31" value="option2" checked/> No
                                                                                <span></span>
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td> Enim eiusmod high life accusamus terry richardson ad squid wolf moon </td>
                                                                    <td>
                                                                        <div class="mt-radio-inline">
                                                                            <label class="mt-radio">
                                                                                <input type="radio" name="optionsRadios41" value="option1" /> Yes
                                                                                <span></span>
                                                                            </label>
                                                                            <label class="mt-radio">
                                                                                <input type="radio" name="optionsRadios41" value="option2" checked/> No
                                                                                <span></span>
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <!--end profile-settings-->
                                                            <div class="margin-top-10">
                                                                <a href="javascript:;" class="btn green"> Save Changes </a>
                                                                <a href="javascript:;" class="btn default"> Cancel </a>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--end col-md-9-->
                                        </div>
                                    </div>
                                    <!--end tab-pane-->
                                </div>
                            </div>
                        </div>

      </div>     
</div>
@endsection                            
@section('afterscripts')
{{Html::script('assets/vendors/bootstrap-fileinput/bootstrap-fileinput.js')}}
{{Html::script('assets/js/JsBarcode.all.min.js')}} 
{{Html::script('assets/js/datepicker.js')}} 
 {{Html::script('assets/vendors/build/js/intlTelInput.js')}}
 <script type="text/javascript">
  //Init jquery Date Picker
        $('.datepicker').datepicker({
             format: 'yyyy-mm-dd',
             autoclose: true,
             orientation: 'bottom'
         });

         // for verify phone country code
      $("#phonenumber").intlTelInput({
        // allowDropdown: false,
        autoHideDialCode: false,
        // autoPlaceholder: "off",
        // dropdownContainer: "body",
        // excludeCountries: ["us"],
        // formatOnDisplay: true,
        // geoIpLookup: function(callback) {
        //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
        //     var countryCode = (resp && resp.country) ? resp.country : "";
        //     callback(countryCode);
        //   });
        // },
        // initialCountry: "auto",
        // nationalMode: false,
        // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
        // placeholderNumberType: "MOBILE",
        // preferredCountries: ['cn', 'jp'],
        separateDialCode: true,
        utilsScript: "{{ asset('assets/vendors/build/js/utils.js') }}"
      });

      $('#phonenumber').on('change', function() {
        $('#hiddenphone').val($("#phonenumber").intlTelInput("getNumber"));
      });

      JsBarcode("#barcode1", $('#barcode').val());

 </script>
@endsection
 
@section('afterstyle')
 {{Html::style('assets/css/datepicker.css')}}
 {{Html::style('assets/vendors/build/css/intlTelInput.css')}}
@endsection