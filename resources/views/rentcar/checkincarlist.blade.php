@extends('layoutmaster')
@section('afterstyle')

<style>
table.dc-table{border-bottom: none !important;}
table.dc-table .dc-action > div{right:7px;}
.page-header-inner{background: #364150;}
.inline-select{display: inline-block; width: auto}
.dc-select{-webkit-appearance: none;-moz-appearance: none;border-radius: 0;padding-right: 22px;padding-left: 3px;color:#333;background-image: url('img/dropdown.png');background-repeat: no-repeat;background-position: right 5px center;cursor: pointer;color:#999;max-width: 103px;}
.dc-select::-ms-expand {display: none;}
.dc-date{width:88px !important;float:none !important;text-align: center;}
.dc-srchform{position: relative; width:243px;}
.dc-form{padding-bottom: 15px;}
.dc-search{padding-right:52px !important;}
.dc-srcgo {border:1px solid #c2cad8;height: 34px;position: absolute;right: 0;top: 0px;width: 40px;background: #fbf7ff !important;color:#ccc;}
.dc-submit {border:1px solid #c2cad8;height: 34px;right: 0;top: 0px;width: 40px;vertical-align: top;background: #fbf7ff !important;color:#ccc;}
.dc-button-fixer{margin-top:1px; margin-left:3px; }
.portlet.light.bordered {padding-top: 20px;}
.dc-filter{width: 280px; display: inline-block; vertical-align: top;}
table.dataTable > tbody > tr.child ul li {display: inline-block;min-width: 33.33%;padding-right: 16px;}
.dc-totitem{text-align: right; margin-top: 10px;}
@media all and (max-width:767px){.dc-totitem{text-align:left;}}
</style>

@endsection
@section('mainbody')

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase">Checkin Car List</span>
                </div>
            </div>
            <div class="portlet-body">
                   <div class="table-toolbar">
                   </div>
                <table class="table table-striped table-bordered table-hover dt-responsive  dc-table" width="100%" >
                    <thead>
                        <tr>
                            <th class="all">#</th>
                            <th class="min-phone-l">Car Barcode</th>
                            <th class="min-tablet">Company</th>
                            <th class="all">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach($car_list as $key=>$item):
                        //dd($item->rccar->rccompany);
                    ?>
                        <tr>
                           <td> {{++$key}} </td>
                            <td>
                                {{$item->rccar->cbarcode}}
                            </td>
                            <td>
                                {{$item->rccar->rccompany->name}}
                            </td>
                            <td>
                                <div class="btn-group pull-right">
                                    <button class="btn green btn-xs btn-outline dropdown-toggle" data-toggle="dropdown">Actions
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-right">
                                        <li>
                                            <a href="{{ route('singlecheckin',['id'=>1, 'lang'=>$lang]) }}">
                                                <i class="fa fa-search"></i> View </a>
                                        </li>
                                        <li>
                                            <a href="">
                                                <i class="fa fa-pencil"></i> Edit </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;">
                                                <i class="icon-tag"></i> Delete </a>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach?>
                    </tbody>
                </table>
                
                
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
  
</div>
@endsection

@section('afterscripts')
{{Html::script('assets/js/JsBarcode.all.min.js')}} 
 <script type="text/javascript">
  JsBarcode("#barcode1", $('#barcode').val());

 </script>
@endsection