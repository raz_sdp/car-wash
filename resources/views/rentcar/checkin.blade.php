@extends('layoutmaster')

@section('mainbody')
<div class="row">
   <div class="col-md-10 col-md-offset-1">
     <div class="portlet light">
        <div class="portlet-title">
            <div class="caption font-blue-sharp">
                <i class="icon-plus font-blue-sunglo"></i>
                <span class="caption-subject bold uppercase">Car Checkin</span>
            </div>
        </div>
        <div class="portlet-body form">
            <div class="row">
            <form id="target">
                <div class="col-md-8">

                    <div class="form-group">
                        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                        <label class="control-label visible-ie8 visible-ie9">Car Barcode</label>
                        <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Car Barcode" id="cbarcode" name="cbarcode" /> 
                    </div>
                </div>
                <div class="col-md-4"> 
                    <div class="form-group">
                        <button type="submit" id="barcodesubmit"class="btn green btn-sm">check car details</button>
                    </div>
                </div>
            </form>    
            </div>        
            <div id="cardetails">
                <div class="row">
                    <div class="col-md-8 profile-info" >
                        <h1 class="font-green sbold uppercase" id="cartitle">{{-- $company->name --}}</h1>
                        <p id="cardesc"> 
                        </p>
                        <p>
                            <a id="carweb" href="javascript:;">{{-- $company->website --}} </a>
                        </p>
                    </div>
                    <!--end col-md-8-->
                    <div class="col-md-4"> 
                       <div class="form-group form-md-checkboxes has-success">
                            <label for="form_control_1"><h3> Services </h3></label>
                            <div class="md-checkbox-list">
                                <div class="md-checkbox">
                                    <input type="checkbox" id="checkbox2_3" class="md-check">
                                    <label for="checkbox2_3">
                                        <span></span>
                                        <span class="check"></span>
                                        <span class="box"></span> Repair </label>
                                </div>
                                <div class="md-checkbox">
                                    <input type="checkbox" id="checkbox2_4" value="0" class="md-check">
                                    <label for="checkbox2_4">
                                        <span></span>
                                        <span class="check"></span>
                                        <span class="box"></span> Washing </label>
                                </div>
                                <div class="form-group" id="washtype">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label class="control-label">Wash Type</label>
                                            <select class="form-control" name="wtype">
                                                <option>Quick Wash(8 min)</option>
                                                <option>Detail Wash(30 min)</option>
                                            </select>
                                        </div>
                                    </div>
                                     <div class="row">
                                        <div class="col-md-12">
                                            <label class="control-label">Segment</label>
                                            <select class="form-control" name="wtype">
                                                <option>mini cars</option>
                                                <option>small cars</option>
                                                <option>medium cars</option>
                                                <option>large cars</option>
                                                <option>executive cars</option>
                                                <option>luxury cars</option>
                                            </select>
                                        </div>
                                    </div>
                                </div> 
                                <div class="md-checkbox">
                                    <input type="checkbox" id="checkbox2_5" class="md-check">
                                    <label for="checkbox2_5">
                                        <span></span>
                                        <span class="check"></span>
                                        <span class="box"></span> Refuel </label>
                                </div>
                            </div>
                        </div>
                    </div>  
                </div>
                <div class="row">
                    <div class="col-md-8 profile-info">
                        <div>
                            <button type="submit" class="btn blue">Submit</button>
                            <button type="button" class="btn default">Cancel</button>
                        </div>                  
                    </div>
                </div> 
            </div>
        	
     </div>
   </div>
</div>      

@endsection
@section('afterscripts')

 <script type="text/javascript">
 $("#cardetails").hide();
 $("#target" ).submit(function( event ) {
  var cbarcode = $('#cbarcode').val();
  if(cbarcode!=''){
  //alert(cbarcode);
  var title='';
  var descrption='';
  var website='';
    
         $.ajax({
            type: "POST",
            url: "{{ route('getcardetails') }}",
            data: {
              'cbarcode': cbarcode,
              '_token': "{{ csrf_token() }}"
            },
            dataType:'JSON',
            success: function (data) {
              
             var cardetails = data.cardetails;
              if(cardetails != null){
                 $("#cardetails").show();
              }

             title+='<h1 class="font-green sbold uppercase">'+cardetails['name']+'</h1>';
             website+='<p>'+cardetails['website']+'</p>';
              $("#cartitle").html(title);
              $("#carweb").html(website);

               if(cardetails['description'] != null){ 
                  descrption+='<p>'+cardetails['description']+'</p>';
                  $("#cardesc").html(descrption);
                }
            }
          });
        }
  event.preventDefault();
});

  
      
      $("#washtype").hide();
      $('#checkbox2_4').on('click', function() {
      if ( $('#checkbox2_4').val() == '0')
      {
        $('#checkbox2_4').val('1')
        $("#washtype").show();
       
      }
      else  
      {
        $('#checkbox2_4').val('0')
        $("#washtype").hide();
      }
    });

 </script>
@endsection