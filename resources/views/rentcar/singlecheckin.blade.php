@extends('layoutmaster')
@section('mainbody')
<div class="row">
   <div class="col-md-10 col-md-offset-1">
     <div class="portlet light">
        <div class="portlet-title">
            <div class="caption font-blue-sharp">
                <i class="icon-plus font-blue-sunglo"></i>
                <span class="caption-subject bold uppercase">{{$car_data->cbarcode}}</span>
            </div>
        </div>
        <div class="portlet-body form">
        	<div class="row">
                <div class="col-md-6 profile-info">
                    <h1 class="font-green sbold uppercase">{{$car_data->rccompany->name}}</h1>

                    <p>
                        <a href="javascript:;">{{$car_data->rccompany->website}} </a>
                    </p>
                </div>
                <!--end col-md-8-->
                <div class="col-md-6"> 
                 <h4 class="caption-subject font-blue bold uppercase">Services</h4>
                    <ol>
                        <?php
                        foreach($process_types as $item):
                        ?>
                        <li>{{$item->rcprocesstype->name}}
                            <button type="button" class="btn btn-primary" style="padding: 1px 1px 1px 1px">start</button>
                        </li>

                        <!--<li>Repair
                            <button type="button" class="btn btn-primary" style="padding: 1px 1px 1px 1px">start</button>
                        </li>
                        <li>Washing
                            <ol type="i">
                                <li style="padding-bottom: 10px;">
                                    <span> Quick Wash(8 min) </span>
                                    <button type="button" class="btn btn-primary" style="padding: 1px 1px 1px 1px">start</button>
                                    <button type="button" class="btn btn-success" style="padding: 1px 1px 1px 1px">end</button>
                                </li>
                                <li> 
                                   <span style="padding: 1px 1px 1px 1px"> Detail Wash(30 min) </span>
                                    <button type="button" class="btn btn-primary" style="padding: 1px 1px 1px 1px">start</button>
                                    <button type="button" class="btn btn-success" style="padding: 1px 1px 1px 1px">end</button>
                                </li>
                            </ol>
                        </li>
                        <li>Refuel
                            <button type="button" class="btn btn-primary" style="padding: 1px 1px 1px 1px">start</button>
                            <button type="button" class="btn btn-success" style="padding: 1px 1px 1px 1px">end</button>
                        </li>-->
                        <?php endforeach?>
                    </ol>
                </div>  
            
            </div>
           
     </div>
   </div>
</div>      

@endsection