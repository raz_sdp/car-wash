<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.5
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>@yield('title')</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Metronic Admin Theme #2 for statistics, charts, recent events and reports" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
          @yield('beforestyle')

        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
         {{Html::style('assets/vendors/font-awesome/css/font-awesome.min.css')}}
         {{Html::style('assets/vendors/simple-line-icons/simple-line-icons.min.css')}}
         {{Html::style('assets/vendors/bootstrap/css/bootstrap.min.css')}}
         {{Html::style('assets/vendors/bootstrap-switch/css/bootstrap-switch.min.css')}}

         
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        {{Html::style('assets/vendors/morris/morris.css')}}
        {{Html::style('assets/vendors/fullcalendar/fullcalendar.min.css')}}
        {{Html::style('assets/vendors/jqvmap/jqvmap/jqvmap.css')}}
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        {{Html::style('assets/css/components.min.css')}}
        {{Html::style('assets/css/plugins.min.css')}}
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        {{Html::style('assets/vendors/layout2/css/layout.min.css')}}
        {{Html::style('assets/vendors/layout2/css/themes/blue.min.css')}}
        {{Html::style('assets/vendors/layout2/css/custom.min.css')}}

        {{Html::style('assets/vendors/jquery-file-upload/css/jquery.fileupload.css')}}
        {{Html::style('assets/vendors/datatables/datatables.min.css')}}
        {{Html::style('assets/vendors/datatables/plugins/bootstrap/datatables.bootstrap.css')}}
        {{Html::style('assets/vendors/datepicker/datepicker.css')}}
        
       {{Html::style('assets/css/login.min.css')}}
         
       {{Html::style('assets/css/profile-2.min.css')}}  
        {{Html::style('assets/style.css')}}
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->
       @yield('afterstyle')  