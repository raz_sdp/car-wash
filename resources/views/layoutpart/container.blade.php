
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            @include('layoutpart.sidebar')
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <div class="row">
                         <div class="col-md-12">
                           <div class="portlet light bordered">
                               @include('layoutpart.message')
                            @yield('mainbody')
                           </div>
                         </div>
                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
       