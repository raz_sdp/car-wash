@yield('beforescripts')

 {{Html::script('assets/vendors/respond.min.js')}} 
 {{Html::script('assets/vendors/excanvas.min.js')}} 
 {{Html::script('assets/vendors/ie8.fix.min.js')}} 
<![endif]-->
            {{Html::script('assets/js/jquery.min.js')}} 
            {{Html::script('assets/vendors/bootstrap/js/bootstrap.min.js')}} 
            {{Html::script('assets/js/js.cookie.min.js')}} 
            {{Html::script('assets/vendors/jquery-slimscroll/jquery.slimscroll.min.js')}}
            {{Html::script('assets/js/jquery.blockui.min.js')}}
            {{Html::script('assets/vendors/bootstrap-switch/js/bootstrap-switch.min.js')}}
            <!-- END CORE PLUGINS -->
            <!-- BEGIN PAGE LEVEL PLUGINS -->
            {{Html::script('assets/js/moment.min.js')}} 
            {{Html::script('assets/vendors/morris/morris.min.js')}} 
            {{Html::script('assets/vendors/morris/raphael-min.js')}} 
            {{Html::script('assets/vendors/counterup/jquery.waypoints.min.js')}} 
            {{Html::script('assets/vendors/counterup/jquery.counterup.min.js')}} 
            {{Html::script('assets/vendors/fullcalendar/fullcalendar.min.js')}} 
            {{Html::script('assets/vendors/horizontal-timeline/horizontal-timeline.min.js')}} 
            {{Html::script('assets/vendors/flot/jquery.flot.min.js')}} 
            {{Html::script('assets/vendors/flot/jquery.flot.resize.min.js')}} 
            {{Html::script('assets/vendors/flot/jquery.flot.categories.min.js')}} 
            {{Html::script('assets/vendors/jquery-easypiechart/jquery.easypiechart.min.js')}} 
            {{Html::script('assets/js/jquery.sparkline.min.js')}} 
            {{Html::script('assets/vendors/jqvmap/jqvmap/jquery.vmap.js')}}
            {{Html::script('assets/vendors/jqvmap/jqvmap/maps/jquery.vmap.russia.js')}} 
            {{Html::script('assets/vendors/jqvmap/jqvmap/maps/jquery.vmap.world.js')}}
            {{Html::script('assets/vendors/jqvmap/jqvmap/maps/jquery.vmap.europe.js')}}
            {{Html::script('assets/vendors/jqvmap/jqvmap/maps/jquery.vmap.germany.js')}}
            {{Html::script('assets/vendors/jqvmap/jqvmap/maps/jquery.vmap.usa.js')}}
            {{Html::script('assets/vendors/jqvmap/jqvmap/data/jquery.vmap.sampledata.js')}}
            <!-- END PAGE LEVEL PLUGINS -->
            <!-- BEGIN THEME GLOBAL SCRIPTS -->
            {{Html::script('assets/js/app.min.js')}}  
            <!-- END THEME GLOBAL SCRIPTS -->
            <!-- BEGIN PAGE LEVEL SCRIPTS -->
            {{Html::script('assets/js/dashboard.min.js')}}  
            <!-- END PAGE LEVEL SCRIPTS -->
            <!-- BEGIN THEME LAYOUT SCRIPTS -->
            {{Html::script('assets/vendors/layout2/scripts/layout.min.js')}} 
            {{Html::script('assets/vendors/layout2/scripts/demo.min.js')}}   

            {{Html::script('assets/js/datatable.js')}}
            {{Html::script('assets/vendors/datatables/datatables.min.js')}}
            {{Html::script('assets/vendors/datatables/plugins/bootstrap/datatables.bootstrap.js')}}
            {{Html::script('assets/js/table-datatables-responsive.js')}}
            {{Html::script('assets/vendors/datepicker/datepicker.js')}}  
            
              {{Html::script('assets/js/login.min.js')}}  
            <script>
                $(document).ready(function()
                {
                    $('#clickmewow').click(function()
                    {
                        $('#radio1003').attr('checked', 'checked');
                    });
                })
            </script>
            @yield('afterscripts')
    </body>

</html>